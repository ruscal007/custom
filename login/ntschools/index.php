
<html>
		<style type="text/css">
			html {
			  background-size:100%;
			  background-repeat:no-repeat;
			  -webkit-background-size: cover;
			  -moz-background-size: cover;
			  -o-background-size: cover;
			}
			h1 {
				font-family: "Century Gothic", CenturyGothic, AppleGothic, sans-serif;
				font-size: 24px;
				font-style: normal;
				font-variant: normal;
				font-weight: bold;
				line-height: 26.4px;
			}
			h2 {
				font-family: "Century Gothic", CenturyGothic, AppleGothic, sans-serif;
				font-size: 14px;
				font-style: normal;
				font-variant: normal;
				font-weight: bold;
				line-height: 15.4px;
			}			
		</style>
<body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
		$(document).ready(function ()
		{
			$('html').css('background-image', 'url(images/background.jpg)');
		});	
	

</script>
<form action="https://www.outpostcentral.com/remote/login.aspx" method="post" id="FrmMain" name="FrmMain">
<br>
<h1 align="center">Welcome to NT Schools Smart Portal Login</h1><br>

<table align="center">
<tr><td colspan='2'><h1>Cumulative Data for All Schools</h1></td></tr>
<tr><td><img src='images/solaricon.png' height='50%'></td><td><h2>Solar Power Production</h2> <br>Today: 134 KWH <br>This Month : 35 MWH  <br> This Year : 56 GWH <br> All Time : 4563 GWH</td></tr>
<tr><td><img src='images/gridicon.png' height='50%'></td><td><h2>Power Consumed from Grid</h2> <br>Today: 54 KWH <br>This Month : 25 MWH  <br> This Year : 36 GWH <br> All Time : 2263 GWH</td></tr>
</table>


<h1 align="center">Access Energy Data Here</h2>
<h2 align="center">Enter your login details</h2>

<table align="center">
<tr>
<td>Username</td>
<td><input id="txtusername" name="txtusername" type="text" /></td>
</tr>
<tr>
<td>Password</td>
<td><input id="txtpassword" name="txtpassword" type="password" /></td>
</tr>
<tr>
<td colspan="2" align="right"><input id="btnlogin" type="submit" value="Login" /></td>
</tr>
</table>
<input type="hidden" id="clt" name="clt" value="" />
</div>
<br>
<div align="center" class="loginFooter">
<a href="http://www.carbonetix.com.au">Carbonetix.com.au</a>
</div>
</form>

</body>
</html>
