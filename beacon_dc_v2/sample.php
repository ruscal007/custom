<?php include("dbconnect.php") ?>
<gauge>

<!-- large gauge -->
	<circle x='165' y='170' radius='150' fill_color='555555' fill_alpha='100' line_thickness='6' line_color='333333' line_alpha='90' />
	<circle x='165' y='170' radius='140' start='240' end='480' fill_color='ffffff' fill_alpha='100' line_thickness='4' line_alpha='20' />
	<circle x='165' y='170' radius='134' start='50' end='115' fill_color='00AB0D' fill_alpha='100' />
	<circle x='165' y='170' radius='120' start='240' end='480' fill_color='ffffff' fill_alpha='100' />
	<circle x='165' y='170' radius='80' fill_color='333333' fill_alpha='100' line_alpha='0' />
	<circle x='165' y='170' radius='130' start='130' end='230' fill_color='333333' fill_alpha='100' line_alpha='0' />
	
	<?php
	//these are PHP functions that generate the XML to to draw radial ticks and numbers
	//any script language can be used to generate the XML code like this
	RadialTicks( 165, 170, 120, 15, 250, 387, 8, 8, "000000" );
	//RadialTicks( 165, 170, 120, 15, 263, 400, 10, 4, "000000" );
	RadialTicks( 165, 170, 120, 15, 55, 110, 3, 4, "ffffff" );
	//RadialNumbers( 165, 170, 120, 0, 100, 245, 465, 11, 14, "444444" );
		RadialNumbers( 165, 170, 120, 0, 70, 245, 380, 8, 14, "444444" );
		RadialNumbers( 165, 170, 120, 80, 100, 50, 105, 3, 14, "444444" );
	?>
	
	
	<?php
		$strSQLsolar = "Select * FROM tbl_aggregated_kwh_287 WHERE dt_date >= '2016-05-11 00:00:00'";
		$query_solar = mysql_query($strSQLsolar);
		while ($rsSolar = mysql_fetch_array($query_solar)){
			$kw = $rsSolar['n_kw'];
		}
		
		if ($kw < 0)
			$kw =0;
		
		if ($kw < 1)
			$gaugeLocation = $kw-10;
		
		else if ($kw <=10)
			$gaugeLocation = $kw-4;
		
		else if ($kw <=20)
			$gaugeLocation = $kw+5;
		
		else if ($kw <=30)
			$gaugeLocation = $kw+18;
		
		else if ($kw <=40)
			$gaugeLocation = $kw+25;
		
		else if ($kw <=50)
			$gaugeLocation = $kw+38;
		
		else if ($kw <=60)
			$gaugeLocation = $kw+46;	
		
		else if ($kw <=70)
			$gaugeLocation = $kw+57;
		
		else if ($kw <=80)
			$gaugeLocation = $kw+73;
		
		else if ($kw <=90)
			$gaugeLocation = $kw+92;
		
		else if ($kw <=100)
			$gaugeLocation = $kw+110;
		
	
	echo "<rotate x='165' y='170' start='-100' span='".$gaugeLocation."' step='5' shake_frequency='100' shake_span='1' shadow_alpha='15'>"
		."<rect x='165' y='40' width='4' height='100' fill_color='00AB0D' fill_alpha='90' line_alpha='0' />"
	."</rotate>";
	?>
	
	<circle x='165' y='170' radius='30' fill_color='111111' fill_alpha='100' line_thickness='5' line_alpha='50' />
	<text x='115' y='210' width='100' size='18' color='ffffff' alpha='70' align='center'>kW Solar</text>
	<?php
	echo "<text x='90' y='235' width='150' size='32' color='ffffff' alpha='70' align='center'>".round($kw,2)." kW</text>";
	?>
	
	
	
	<?php
	//====================================
	//PHP function that generates the XML code to draw radial ticks
	function RadialTicks ( $x_center, $y_center, $radius,  $length, $start_angle, $end_angle, $ticks_count, $thickness, $color ){
		
		for ( $i=$start_angle; $i<=$end_angle; $i+=($end_angle-$start_angle)/($ticks_count-1) ){
			echo "	<line x1='".($x_center+sin(deg2rad($i))*$radius)."' y1='".($y_center-cos(deg2rad($i))*$radius)."' x2='".($x_center+sin(deg2rad($i))*($radius+$length))."' y2='".($y_center-cos(deg2rad($i))*($radius+$length))."' thickness='".$thickness."' color='".$color."' />";
		
		}
	}
	//====================================
	//PHP function that generates the XML code to draw radial numbers
	function RadialNumbers ( $x_center, $y_center, $radius,  $start_number, $end_number, $start_angle, $end_angle, $ticks_count, $font_size, $color ){
		
		$number=$start_number;
		
		for( $i=$start_angle; $i<=$end_angle; $i+=($end_angle-$start_angle)/($ticks_count-1) ){
			echo "	<text x='".($x_center+sin(deg2rad($i))*$radius)."' y='".($y_center-cos(deg2rad($i))*$radius)."' width='200' size='".$font_size."' color='".$color."' align='left' rotation='".$i."'>".$number."</text>";
			$number += ($end_number-$start_number)/($ticks_count-1);
		
		}
	}
	//===========
	?>
</gauge>
<?php include("closedbconnection.php") ?>