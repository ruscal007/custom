<?php

header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
?>

<HTML>
<head>
<script src="./js/gauge.min.js"></script>
<script src="./js/moment-with-locales.min.js"></script>	
<script src="./js/Chart.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>


<script>

//***************************************************************************************************************************

	var D_ID = 'op21107';
	var Interval_Data = [];
	
//	inputs.push('KWH_Day_Total_Export_Solar');
//	inputs.push('KWH_Interval_Export_Solar');

	var data_array = [];
	
	

	Interval_Data['KWH_Day_Total_Export_Solar'] = [];
	Interval_Data['KWH_Interval_Export_Solar'] = [];
	Interval_Data['KW_Instant'] = [];
	
	function get_Op_Interval_Records(input,start_time,end_time) {
		var start_date = new Date(start_time);
		var end_date = new Date(end_time);
		
		var dateFrom =  start_date.getDate() + "/" + (start_date.getMonth() + 1) + "/" + start_date.getFullYear() + "+00:00:00" ;
		var dateTo =  end_date.getDate() + "/" + (end_date.getMonth() + 1) + "/" + end_date.getFullYear() + "+00:00:00" ;
		var url = "get_Outpost.php?inputName=" + input + "&dateFrom=" + dateFrom +"&dateTo=" + dateTo +"&outpostID="+ D_ID + "&format=xml";
		
		$.ajax({
			type: "GET",
			url: url,
			dataType: "xml",
			success: function(xml) {
					Site_Name = $(xml).find('opdata\\:site, site').children('name').text();
					if ( /"/.test( Site_Name ) ){
						Site_Name = Site_Name.match( /"(.*?)"/ )[1];
					} 					
					Last_Upload = $(xml).find('opdata\\:logger, logger').children('last_telemetry').text();
					Log_Interval = $(xml).find('opdata\\:input, input').children('logInterval').text();
					Log_Interval = parseFloat(Log_Interval);

					Interval_Data[input].Log_Interval = Log_Interval;
					
					var dateChanged = true;
					var oldDate;
					
					$(xml).find('opdata\\:record, record').each(function(){
						if (typeof oldDate != 'undefined') {
							if (oldDate != $(this).find('date').text().substr(0, $(this).find('date').text().indexOf('T'))) {
								dateChanged = true;
							} else {
								dateChanged = false;
							}
						}

						Interval_Data[input][$(this).find('date').text()] = parseFloat($(this).find('value').text());
						oldDate = $(this).find('date').text().substr(0, $(this).find('date').text().indexOf('T'));
					});
			},
			error: function( error )
			{
				setTimeout(get_Op_Interval_Records(input,start_time,end_time),3000);
			}
		}); 
	}
	
	function get_Site_Data(period) {
		switch(period) {
		case 'Today':
			var start_date = new Date(new Date().getTime());
			start_date.setHours(0,0,0,0);
			var start_time = start_date.getTime();
			
			var end_date = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
			end_date.setHours(0,0,0,0);
			var end_time = end_date.getTime();
			
			get_Op_Interval_Records('KWH_Interval_Export_Solar',start_time,end_time);
			get_Op_Interval_Records('KW_Instant',start_time,end_time);
		break;
		case 'Past 7 Days':
			var start_date = new Date(new Date().getTime() - 7 * 24 * 60 * 60 * 1000);
			start_date.setHours(0,0,0,0);
			var start_time = start_date.getTime();
			
			var end_date = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
			end_date.setHours(0,0,0,0);
			var end_time = end_date.getTime();
			get_Op_Interval_Records('KWH_Interval_Export_Solar',start_time,end_time);
		break;
		case 'This Month':
			var start_date = new Date(new Date().getTime() - 7 * 24 * 60 * 60 * 1000);
			start_date.setDate(1);
			start_date.setHours(0,0,0,0);
			var start_time = start_date.getTime();
			
			var end_date = new Date(new Date().getTime());
			end_date.setHours(0,0,0,0);
			var end_time = end_date.getTime();
			get_Op_Interval_Records('KWH_Day_Total_Export_Solar',start_time,end_time);
		break;
		case 'This Year':
			var start_date = new Date(new Date().getTime());
			start_date.setDate(1);
			start_date.setMonth(0);
			start_date.setHours(0,0,0,0);
			var start_time = start_date.getTime();
			
			var end_date = new Date(new Date().getTime());
			end_date.setDate(0);
			end_date.setHours(0,0,0,0);
			var end_time = end_date.getTime();
			get_Op_Interval_Records('KWH_Day_Total_Export_Solar',start_time,end_time);
		break;
		case 'All Time':
			var start_date = new Date(new Date().getTime());
			start_date.setHours(0,0,0,0);
			start_date.setFullYear(1970);
			var start_time = start_date.getTime();
			
			var end_date = new Date(new Date().getTime());
			end_date.setHours(0,0,0,0);
			var end_time = end_date.getTime();
			get_Op_Interval_Records('KWH_Day_Total_Export_Solar',start_time,end_time);
		break;
		default:
		}
	}	

	function get_point_sum(total, point) {
		return total + point.y;
	}

	function get_sum(total, point) {
		return total + point;
	}
	
	function update_radial(index) {
			if (typeof index == 'undefined') {
				index = 10;
			}
		
			if (index < 1) {
				index = 10;
			}
			
			if (typeof data_array['KW_Instant'] != 'undefined') {
				if (data_array['KW_Instant'].length > 5) {
					var radial = document.gauges[0];
					radial.update({value: data_array['KW_Instant'][data_array['KW_Instant'].length-index].y});
				}
			}
			setTimeout(function(){update_radial(index - 1)},5000);
	}
	
	function update_screen() {
			var start_today = new Date();
			start_today.setHours(0,0,0,0); //Start of today
			var start_today_time = start_today.getTime();
			
			var start_tomorrow = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
			start_tomorrow.setHours(0,0,0,0); //Start of Tomorrow
			var start_tomorrow_time = start_tomorrow.getTime();
			
			
			data_array['KW_Instant'] = [];
			
			for (var key in Interval_Data['KW_Instant']) {
				if (Interval_Data['KW_Instant'].hasOwnProperty(key)) {
					var x_moment = moment(key,"YYYY-MM-DDTHH:mm:ss");
					if (x_moment.isValid()) {
						var start_date = moment();
						start_date.set({hour:0,minute:0,second:0,millisecond:0});
						var end_date = moment().add(1, "days");
						end_date.set({hour:0,minute:0,second:0,millisecond:0});
						if (x_moment.isBetween(start_date,end_date)) {
							data_array['KW_Instant'].push({x:x_moment,y:parseFloat(Interval_Data['KW_Instant'][key])});
						}
					}
				}
			}
//			data_array['KWH_Interval_Export_Solar'].sort(function (a, b) {
//				return a.y - b.y;
//			});

//			if (typeof data_array[show_inputs[i]][0] != 'undefined') {
//				data_array[show_inputs[i]][0]["label"] = "min";
//				data_array[show_inputs[i]][data_array[show_inputs[i]].length-1]["label"] = "max";
//			}
			data_array['KW_Instant'].sort(function (a, b) {
				return a.x - b.x;
			});

			function getSum(total, num) {
				return total + num;
			}			
			
			var solar_total_today = 0;
			var solar_total_alltime = 0;
			var solar_total_year = 0;
			var solar_total_month = 0;
			
			if (!jQuery.isEmptyObject(Interval_Data["KWH_Interval_Export_Solar"])) {
				for (var key in Interval_Data['KWH_Interval_Export_Solar']) {

					if (Interval_Data['KWH_Interval_Export_Solar'].hasOwnProperty(key)) {
						var x_moment = moment(key,"YYYY-MM-DDTHH:mm:ss");
						// code for KWH this month, this year , all time
						if (x_moment.isValid()) {
							var end_date = moment();							
							
							if (x_moment.isSame(end_date,'day')) {
								solar_total_today += parseFloat(Interval_Data['KWH_Interval_Export_Solar'][key]);
							}
							
						}
					}

				}				
			} 
			
			
			
			if (!jQuery.isEmptyObject(Interval_Data["KWH_Day_Total_Export_Solar"])) {
				for (var key in Interval_Data['KWH_Day_Total_Export_Solar']) {

					if (Interval_Data['KWH_Day_Total_Export_Solar'].hasOwnProperty(key)) {
						var x_moment = moment(key,"YYYY-MM-DDTHH:mm:ss");
						// code for KWH this month, this year , all time
						if (x_moment.isValid()) {
							var start_date = moment(1, "DD")  // first day of this month
							
							var end_date = moment();
							
							end_date.endOf("month"); // last day of this month
							if (x_moment.isBetween(start_date,end_date, null, '[]')) {
								solar_total_month += parseFloat(Interval_Data['KWH_Day_Total_Export_Solar'][key]);
							}
							var start_date = moment(1, "MM")  // first day of this month
							
							var end_date = moment();
							end_date.endOf("year"); // last day of this month
							if (x_moment.isBetween(start_date,end_date, null, '[]')) {
								solar_total_year += parseFloat(Interval_Data['KWH_Day_Total_Export_Solar'][key]);
							}
							solar_total_alltime += parseFloat(Interval_Data['KWH_Day_Total_Export_Solar'][key]);											
						}
					}

				}				
			} 
			
			solar_total_month += solar_total_today;
			solar_total_year += solar_total_today;
			solar_total_alltime += solar_total_today;

			var solartxt = document.getElementById('solar_today');
			
			solartxt.innerText = Math.round(solar_total_today * 100) / 100 + " KWH";
			
			var solartxt = document.getElementById('solar_month');
			solartxt.innerText = Math.round(solar_total_month * 100) / 100 + " KWH";

			var solar_month = Math.round(solar_total_month * 100) / 100;
			
			if (solar_total_month < 1000) {
				solartxt.innerText = solar_month + " KWH";
			} else if (solar_total_month < (1000 * 1000)) {
				solar_month = Math.round((solar_total_month/1000) * 100) / 100;
				solartxt.innerText = solar_month + " MWH";
			}

			
			
			
			var solartxt = document.getElementById('solar_year');
			var solar_year = Math.round(solar_total_year * 100) / 100;

			if (solar_total_year < 1000) {
				solartxt.innerText = solar_year + " KWH";
			} else if (solar_total_year < (1000 * 1000)) {
				solar_year = Math.round((solar_total_year/1000) * 100) / 100;
				solartxt.innerText = solar_year + " MWH";
			}

			var solartxt = document.getElementById('solar_all');
			
			

			var solar_all = Math.round(solar_total_alltime * 100) / 100
			
			if (solar_total_alltime < 1000) {
				solartxt.innerText = solar_all + " KWH";
			} else if (solar_total_alltime < (1000 * 1000)) {
				solar_year = Math.round((solar_total_alltime/1000) * 100) / 100;
				solartxt.innerText = solar_year + " MWH";
			}else if (solar_total_alltime < (1000 * 1000 * 1000)) {
				solar_year = Math.round((solar_total_alltime/(1000 * 1000)) * 100) / 100;
				solartxt.innerText = solar_year + " GWH";
			}

			
			



			
			var ctx = document.getElementById('cvs').getContext('2d');
			if (typeof window.myLineChart != 'undefined') {
				window.myLineChart.destroy();
			}
			window.myLineChart = new Chart(ctx, {
				type: 'line',
				data: {
					datasets: [{					
						label: "Solar Generation",
						backgroundColor: 'rgba(50, 185, 45,1)',
						borderColor: 'rgba(50, 185, 45,1)',
						data: data_array["KW_Instant"],
						fill: false
					}]
				},
				options: {
					elements: {
						point:{
							radius: 0
					}},
					animation:false,
					responsive: true,
					title:{
						display:true,
						fontSize: 20,
						text:'Solar Generation'
//						text:Site_Name + ' KW Profile past 7 days'
					},
					tooltips: {
						mode: 'index',
						intersect: false,
					},
					hover: {
						mode: 'nearest',
						intersect: true
					},
					scales: {
						xAxes: [{
							type: "time",
							time: {
								min: start_today_time,
								max: start_tomorrow_time,
								display: true
							}
							
						}],
						yAxes: [{
							display: true,
							scaleLabel: {
								display: true,
								labelString: 'KW'
							},
							ticks : {
								min : 0,
								max : 100
							}
						}]
					}
				}
			});
	}

	
	function update_time() {
			var cur_time = document.getElementById('cur_time');
			var cur_date = document.getElementById('cur_date');
			
			cur_time.innerText = moment().format('h:mm:ss a');
			cur_date.innerText = moment().format('DD-MM-YYYY');
	}
	
	
	$(document).ready(function ()
	{
		
		var radial = new RadialGauge({
			renderTo: 'Radial_KW',
			width: 355,
			height: 355,
			units: 'KW',
			title: false,
			value: 0,
			minValue: 0,
			maxValue: 100,
			majorTicks: [
				'0','10','20','30','40','50','60','70','80','90','100'
			],
			minorTicks: 2,
			strokeTicks: false,
			highlights: [
				{ from: 0, to: 20, color: 'rgba(0,255,0,.15)' },
				{ from: 20, to: 40, color: 'rgba(255,255,0,.15)' },
				{ from: 40, to: 60, color: 'rgba(255,30,0,.25)' },
				{ from: 60, to: 80, color: 'rgba(255,0,225,.25)' },
				{ from: 80, to: 100, color: 'rgba(0,0,255,.25)' }
			],
			colorPlate: '#222',
			colorMajorTicks: '#f5f5f5',
			colorMinorTicks: '#ddd',
			colorTitle: '#fff',
			colorUnits: '#ccc',
			colorNumbers: '#eee',
			colorNeedle: 'rgba(240, 128, 128, 1)',
			colorNeedleEnd: 'rgba(255, 160, 122, .9)',
			valueBox: true,
			animationRule: 'bounce',
			animationDuration: 500
		});
		radial.draw();
		
		get_Site_Data('All Time');	
		get_Site_Data('Today');
		setInterval( function() { get_Site_Data('Today'); }, 300000 );
		setInterval( function() { get_Site_Data('All Time'); }, 21600000 );
	
		setInterval(update_screen,60000);
		
		update_radial();
		setInterval(update_time,1000);
	});	
	
	
	
	
	</script>
	


</head>
<BODY bgcolor="#000000" style="font-family:arial">

<div style="float:left; width:100%; padding-bottom:2%">
	<div style="width:75%; text-align:center; float:left"><h1 style="font-size:38px; color:orange; margin:1px">Beacon Lighting DC 100kW Solar System</h2></div>
	<div style="width:25%; text-align:center; float:left">
		<img src="images/CarbonetiX-logo-with-tagline-white-200px.png" style="float:right; width:150px" />
		<h2 id="txt" style="color:white; margin:1px"><div id='cur_time'></div></h2> <h3 style="color:white; margin:1px"><div id='cur_date'></div></h3>		
	</div>
</div>

<div style="float:left; width:100%; padding-bottom:1%;"><img style="width:100%" src="images/beacon-solar-header.JPG" /></div>

<div style="float:left; width:25%; text-align:center">
<canvas id="Radial_KW">
></canvas>
<script>
</script>
</div>

<div style="float:left; width:45%; text-align:center">
<iframe width="450" height="350" src="https://www.youtube.com/embed/Ih8HWeHup9c?autoplay=1&loop=1&playlist=Ih8HWeHup9c&mute=1&rel=0&showinfo=0&controls=0&autohide=1" frameborder="0" ></iframe>
</div>
<div style="float:left; width:30%; text-align:center;">
	<canvas  id='cvs' width='380' height='350'>
	</canvas>
</div>
<noscript>
	<P>This content requires JavaScript.</P>
</noscript>
</div>
<div style="padding-top:20px; padding-bottom:20px; float:left; width:100%; text-align:center">
<h3 style="font-size:24px; display:inline-block; color:white; margin:0; padding:0;">Today</h3>
<h3 id="solar_today" style="border:2px solid #00AB0D; font-size:32px; display:inline-block; color:white; margin:0; padding:5px; margin-right:25px">
</h3>

<h3 style="font-size:24px; display:inline-block; color:white; margin:0; padding:0;">This Month</h3>
<h3 id="solar_month" style="border:2px solid #00AB0D;font-size:32px; display:inline-block; color:white; margin:0; padding:5px; margin-right:25px">
</h3>

<h3 style="font-size:24px; display:inline-block; color:white; margin:0; padding:0;">This Year</h3>
<h3 id="solar_year" style="border:2px solid #00AB0D;font-size:32px; display:inline-block; color:white; margin:0; padding:5px; margin-right:25px">
</h3>

<h3 style="font-size:24px; display:inline-block; color:white; margin:0; padding:0;">Since Install</h3>
<h3 id="solar_all" style="border:2px solid #00AB0D;font-size:32px; display:inline-block; color:white; margin:0; padding:5px; margin-right:25px">
</h3>
</div>
<div style="float:left; width:100%; padding-top:2%"><img style="width:100%" src="images/beacon-solar-banner.JPG" /></div>
</BODY>

</HTML>
