<?php include("dbconnect.php") ?>
<chart>

	<axis_category skip='10' margin='false' size='10' color='ffffff' alpha='100' orientation='horizontal' />
	<axis_ticks value_ticks='false' category_ticks='true' major_thickness='2' minor_thickness='1' minor_count='1' major_color='000000' minor_color='222222' position='inside'/>
	<axis_value size='10' color='ffffff' alpha='100' steps='2' prefix='' suffix='' decimals='0' separator='' show_min='false' />
	
	

	<chart_border color='000000' top_thickness='0' bottom_thickness='2' left_thickness='0' right_thickness='0' />
	<chart_data>
		<row>
			<null/>
			<?php
			$solar_results = Array();
			$grid_results = Array();
			
			$start_date = date("Y-m-d")." 00:00:00";
			$count_date_ts = strtotime($start_date);
			$strSQLsolar = "Select * FROM tbl_aggregated_kwh_287 WHERE dt_date >= '".date("Y-m-d")." 00:00:00' ORDER BY dt_date ASC";
			$query_solar = mysql_query($strSQLsolar);
			
			while ($rsSolar = mysql_fetch_array($query_solar)){
				$solar_results[$rsSolar['dt_date']] = $rsSolar['n_kw'];
			}	
			
//			echo $count_date_ts . " ";
//			echo strtotime('+1 day', strtotime($start_date));
			while ($count_date_ts < strtotime('+1 day', strtotime($start_date))) {
				$arrayindex = date("Y-m-d H:i:s",$count_date_ts);
				
//				if (array_key_exists($arrayindex,$solar_results)){
//					echo "<string>".date("H:m A", strtotime($rsSolar['dt_date']))."</string>";
					
//				} else {
					echo "<string>".date("H:i A", $count_date_ts)."</string>";
					echo "<string></string>";
					echo "<string></string>";
					echo "<string></string>";
//				}
				$count_date_ts = strtotime('+1 hour', $count_date_ts);
			}					
			
			?>
		</row>
		<row>
			<string>Solar</string>
			<?php
			
			
			//$strSQLsolar = "Select * FROM tbl_aggregated_kwh_287 WHERE dt_date >= '".date("Y-m-d")." 00:00:00'  ORDER BY dt_date ASC";
			//$query_solar = mysql_query($strSQLsolar);
			
			
			$count_date_ts = strtotime($start_date);
			
			while ($count_date_ts < strtotime('+1 day', strtotime($start_date))) {
				$arrayindex = date("Y-m-d H:i:s",$count_date_ts);
				
				if (array_key_exists($arrayindex,$solar_results)){
					//if ($solar_results[$arrayindex] > 2.5)
						echo "<number>".$solar_results[$arrayindex]."</number>";
					//else
					//	echo "<null/>";
				} else {
					echo "<null/>";
				}
				
				$count_date_ts = strtotime('+15 minutes', $count_date_ts);
			}			
			?>
		</row>
		<row>
			<string>Load</string>
			<?php
			
			
			
			$strSQLgrid = "Select * FROM tbl_aggregated_kwh_286 WHERE dt_date >= '".date("Y-m-d")." 00:00:00' ORDER BY dt_date ASC";
			$query_grid = mysql_query($strSQLgrid);
						
			
			while ($rsGrid = mysql_fetch_array($query_grid)){
				$grid_results[$rsGrid['dt_date']] = $rsGrid['n_kw'];
			}	
			
			
			$count_date_ts = strtotime($start_date);
			while ($count_date_ts < strtotime('+1 day', strtotime($start_date))) {
				$arrayindex = date("Y-m-d H:i:s",$count_date_ts);
				
				if (array_key_exists($arrayindex,$grid_results)){
						echo "<number>".$grid_results[$arrayindex]."</number>";
				} else {
					echo "<null/>";
				}
				
				$count_date_ts = strtotime('+15 minutes', $count_date_ts);
			}						
			
			?>
		</row>
	</chart_data>
	<chart_grid_h alpha='5' color='000000' thickness='5' />
	
	<chart_pref rotation_x='15' rotation_y='0' />
	<chart_rect x='30' y='50' width='335' height='250' positive_alpha='0' />
	<chart_transition type='zoom' delay='0' duration='1' order='all' />
	<chart_type>area</chart_type>

	<legend shadow='high' x='30' y='10' width='330' height='35' margin='10' fill_alpha='10' fill_color='000000' line_alpha='0' line_thickness='0' bullet='circle' size='22' color='ffffff' alpha='85' />
	
	<series_color>
		<color>88ff00</color>
		<color>0088ff</color>
	</series_color>

</chart>
<?php include("closedbconnection.php") ?>