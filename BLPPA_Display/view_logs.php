<HTML>
<HEAD>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-bootgrid/1.3.1/jquery.bootgrid.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</HEAD>
<BODY>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-bootgrid/1.3.1/jquery.bootgrid.min.js"></script>

<script>
    $( document ).ready(function() {
		$("#logtable").bootgrid();
    });
</script>


		<table id="logtable" class="table table-condensed table-hover table-striped">
			<thead>
				<tr>
					<th data-column-id="requestDT">Requested Date</th>
					<th data-column-id="outpostID">Outpost ID</th>
					<th data-column-id="requestIP">Requesting IP</th>
					<th data-column-id="dateFrom">Requested Data Date From</th>
					<th data-column-id="dateTo">Requested Data Date To</th>
					<th data-column-id="curl_error">Error Msg if failed to get data</th>
				</tr>
			</thead>
			<tbody>

<?php
$dir = new DirectoryIterator("logs/");

foreach ($dir as $fileinfo) {
    if (!$fileinfo->isDot()) {
		$inp = file_get_contents("logs/" . $fileinfo->getFilename());
		$logs = json_decode($inp,TRUE);
		
		if (is_array($logs)) {
			$index = count($logs);

			while($index) {
				
				if (is_array($logs[$index])) {
?>
				<tr>
					<td><?=$logs[$index]["requestDT"]?></td>
					<td><?=$logs[$index]["outpostID"]?></td>
					<td><?=$logs[$index]["requestIP"]?></td>
					<td><?=$logs[$index]["dateFrom"]?></td>
					<td><?=$logs[$index]["dateTo"]?></td>
					<td><?=$logs[$index]["curl_error"]?></td>
				</tr>
<?php					
				}
				
				--$index;
			}
		
		}

    }
}

?>
			</tbody>
		</table>
</BODY>
</HTML>