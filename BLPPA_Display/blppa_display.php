<?php
//  
//	header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
//	header("Cache-Control: post-check=0, pre-check=0", false);
//	header("Pragma: no-cache");
// http://smartportal.com.au/BLPPA_Display/blppa_display.php?oID=op30862	
	

	
?>
	<!DOCTYPE html
		PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
		"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
	<title>BLPPA Display</title>
		<style type="text/css">
			html {
			  background-size:100%;
			  background-repeat:no-repeat;
			  -webkit-background-size: cover;
			  -moz-background-size: cover;
			  -o-background-size: cover;
			  
			}
			h1 {
				font-family: "Century Gothic", CenturyGothic, AppleGothic, sans-serif;
				font-size: 24px;
				font-style: normal;
				font-variant: normal;
				font-weight: 500;
				line-height: 26.4px;
			}
			h3 {
				font-family: "Century Gothic", CenturyGothic, AppleGothic, sans-serif;
				font-size: 14px;
				font-style: normal;
				font-variant: normal;
				font-weight: 500;
				line-height: 15.4px;
			}
			p {
				font-family: "Century Gothic", CenturyGothic, AppleGothic, sans-serif;
				font-size: 14px;
				font-style: normal;
				font-variant: normal;
				font-weight: 400;
				line-height: 20px;
			}
			blockquote {
				font-family: "Century Gothic", CenturyGothic, AppleGothic, sans-serif;
				font-size: 21px;
				font-style: normal;
				font-variant: normal;
				font-weight: 400;
				line-height: 30px;
			}
			pre {
				font-family: "Century Gothic", CenturyGothic, AppleGothic, sans-serif;
				font-size: 13px;
				font-style: normal;
				font-variant: normal;
				font-weight: 400;
				line-height: 18.5714px;
			}
			body, p{
				font-family:Calibri, verdana;
				font-size:16px;
			}
			
			#title {
				position: absolute;
				top: 50px;
				width: 100%;
			}

			#title h1{
				color: blue;
				text-align:center;
				
			}
			
			#main {
				position: absolute;
				left: 230px;
				top: 150px;
			}
			
			#cars_off_road {
				position: absolute;
				left: 300px;
				top: 735px;
				width : 870px;
				font-size: 50pt;
				color: grey;
			}
			
			#since_install_text {
				font-family: "Century Gothic";
				position: absolute;
				left: 320px;
				top: 685px;
				width : 870px;
				font-size: 30pt;
				color: #4c4c4c;
				
			}
			
			#solar_week {
				font-family: "Century Gothic";
				position: absolute;
				left: 825px;
				top: 640px;
				width : 870px;
				font-size: 17pt;
				color: grey;
				display:inline;
			}

			#solar_month {
				font-family: "Century Gothic";
				position: absolute;
				left: 825px;
				top: 708px;
				width : 870px;
				font-size: 17pt;
				color: grey;
				display:inline;
			}

			#solar_year {
				font-family: "Century Gothic";
				position: absolute;
				left: 825px;
				top: 775px;
				width : 870px;
				font-size: 17pt;
				color: grey;
				display:inline;
			}
			
			#solar_all {
				font-family: "Century Gothic";
				position: absolute;
				left: 825px;
				top: 842px;
				width : 870px;
				font-size: 17pt;
				color: grey;
				display:inline;
			}

			.title {
				display:inline-block;
			}
			
			.num {
				position:absolute;
				display:inline-block;
				left: 600px;
			}
			
			</style>
	</head>
	<script src="./includes/moment-with-locales.min.js"></script>	
	<script src="./includes/Chart.js"></script>
	
	<script>

	var D_ID = '<?=$_REQUEST['ID']?>';
	var display_type = '<?=$_REQUEST['type']?>';
	var Site_Name;
	
	var Interval_Data = [];
	var Monthly_Data = [];
	var Daily_Data = [];
	var KWH_day_total = [];
	var inputs = [];

	var solar_total_today = 0;
	var solar_total_alltime = 0;
	var solar_total_year = 0;
	var solar_total_month = 0;
	var solar_total_week = 0;
	var solar_days_active = 0
	
//	inputs.push('KWH_Interval_Battery');
//	inputs.push('KWH_Interval_Battery_Export');
//	inputs.push('KWH_Interval_Grid');
//	inputs.push('KWH_Interval_Grid_Export');
//	inputs.push('KWH_Interval_HVAC');
//	inputs.push('KWH_Interval_Lighting');
//	inputs.push('KWH_Interval_Power');
	inputs.push('KWH_Interval_Export_Solar');
	inputs.push('KWH_Day_Total_Export_Solar');
	
	for (var i=0;i<inputs.length;i++) {
		Interval_Data[inputs[i]] = [];
		KWH_day_total[inputs[i]] = [];
	}
	
	function get_Op_Interval_Records(input,start_time,end_time) {


		var start_date = new Date(start_time);
		var end_date = new Date(end_time);
		
		var dateFrom =  start_date.getDate() + "/" + (start_date.getMonth() + 1) + "/" + start_date.getFullYear() + "+00:00:00" ;
		var dateTo =  end_date.getDate() + "/" + (end_date.getMonth() + 1) + "/" + end_date.getFullYear() + "+00:00:00" ;
		var url = "get_Outpost.php?inputName=" + input + "&dateFrom=" + dateFrom +"&dateTo=" + dateTo +"&outpostID="+ D_ID + "&format=xml";
		
		$.ajax({
			type: "GET",
			url: url,
			dataType: "xml",
			success: function(xml) {
					solar_total_today = 0;
					solar_total_alltime = 0;
					solar_total_year = 0;
					solar_total_month = 0;
					solar_total_week = 0;
					solar_days_active = 0
					
					Site_Name = $(xml).find('opdata\\:site, site').children('name').text();
				if (!Site_Name.length) { // if site name is not found then lets retry get the data
						setTimeout(function() {
							get_Op_Interval_Records(input,start_time,end_time);
							},((60+Math.floor(Math.random() * 61))*1000));
				}
				else {
					if ( /"/.test( Site_Name ) ){
						Site_Name = Site_Name.match( /"(.*?)"/ )[1];
					} 					
					Last_Upload = $(xml).find('opdata\\:logger, logger').children('last_telemetry').text();
					Log_Interval = $(xml).find('opdata\\:input, input').children('logInterval').text();
					Log_Interval = parseFloat(Log_Interval);
	            
					Interval_Data[input].Log_Interval = Log_Interval;
					
					$(xml).find('opdata\\:record, record').each(function(){
						Interval_Data[input][$(this).find('date').text()] = parseFloat($(this).find('value').text());
					});
					
					if (!jQuery.isEmptyObject(Interval_Data["KWH_Interval_Export_Solar"])) {
						for (var key in Interval_Data['KWH_Interval_Export_Solar']) {
							if (Interval_Data['KWH_Interval_Export_Solar'].hasOwnProperty(key)) {
								var x_moment = moment(key,"YYYY-MM-DDTHH:mm:ss");
								// code for KWH this month, this year , all time
								if (x_moment.isValid()) {
									var end_date = moment();							
									
									if (x_moment.isSame(end_date,'day')) {
										solar_total_today += parseFloat(Interval_Data['KWH_Interval_Export_Solar'][key]);
									}
								}
							}
						}				
					} 
					
					if (!jQuery.isEmptyObject(Interval_Data["KWH_Day_Total_Export_Solar"])) {
						for (var key in Interval_Data['KWH_Day_Total_Export_Solar']) {
			    
							if (Interval_Data['KWH_Day_Total_Export_Solar'].hasOwnProperty(key)) {
								solar_days_active++;
								var x_moment = moment(key,"YYYY-MM-DDTHH:mm:ss");
								// code for KWH this month, this year , all time
								if (x_moment.isValid()) {
									var start_date = moment().startOf("week");  // first day of this month
									
									var end_date = moment();
									
									end_date.endOf("week"); // last day of this month
									if (x_moment.isBetween(start_date,end_date, null, '[]')) {
										solar_total_week += parseFloat(Interval_Data['KWH_Day_Total_Export_Solar'][key]);
									}
									
									
									
									var start_date = moment(1, "DD")  // first day of this month
									
									var end_date = moment();
									
									end_date.endOf("month"); // last day of this month
									if (x_moment.isBetween(start_date,end_date, null, '[]')) {
										solar_total_month += parseFloat(Interval_Data['KWH_Day_Total_Export_Solar'][key]);
									}
									var start_date = moment(1, "MM")  // first day of this month
									
									var end_date = moment();
									end_date.endOf("year"); // last day of this month
									if (x_moment.isBetween(start_date,end_date, null, '[]')) {
										solar_total_year += parseFloat(Interval_Data['KWH_Day_Total_Export_Solar'][key]);
									}
									solar_total_alltime += parseFloat(Interval_Data['KWH_Day_Total_Export_Solar'][key]);											
								}
							}
						}
						Interval_Data['KWH_Day_Total_Export_Solar'][moment().format("YYYY-MM-DDT00:00:00")] = solar_total_today;
					}
					solar_total_week += solar_total_today;
					solar_total_month += solar_total_today;
					solar_total_alltime += solar_total_today;
				}
			},
			error: function( error )
			{
				setTimeout(function() {
					get_Op_Interval_Records(input,start_time,end_time);
				},((60+Math.floor(Math.random() * 61))*1000));
			}
		}); 
	}
	
	function get_Site_Data(input_list,period) {
		switch(period) {
		case 'Today':
			var start_date = new Date(new Date().getTime());
			start_date.setHours(0,0,0,0);
			var start_time = start_date.getTime();
			
			var end_date = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
			end_date.setHours(0,0,0,0);
			var end_time = end_date.getTime();
		
		break;
		case 'Past 7 Days':
			var start_date = new Date(new Date().getTime() - 7 * 24 * 60 * 60 * 1000);
			start_date.setHours(0,0,0,0);
			var start_time = start_date.getTime();
			
			var end_date = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
			end_date.setHours(0,0,0,0);
			var end_time = end_date.getTime();

		break;
		case 'All Time':
			var start_date = new Date(new Date().getTime());
			start_date.setHours(0,0,0,0);
			start_date.setFullYear(1970);
			var start_time = start_date.getTime();
			
			var end_date = new Date(new Date().getTime());
			end_date.setHours(0,0,0,0);
			var end_time = end_date.getTime();

		break;

		default:
		}


		for (var i=0;i<input_list.length;i++) {
			get_Op_Interval_Records(input_list[i],start_time,end_time);
		}
	}	

	function get_point_sum(total, point) {
		return total + point.y;
	}

	function get_sum(total, point) {
		return total + point;
	}
	
	
	function update_screen(current_screen) {
		var mainHTML;
		mainHTML = "";
		
		switch(current_screen) {
		case 1:
			$('html').css('background-image', 'url(images/BG1.jpg)');

			document.body.innerHTML = "";
			setTimeout(function(){update_screen(current_screen+1)},5000)
			break;
		case 2:
			$('html').css('background-image', 'url(images/BG2.jpg)');
			
			document.body.innerHTML = "";
			setTimeout(function(){update_screen(current_screen+1)},5000)
			break;
		case 3:
			$('html').css('background-image', 'url(images/BG3.jpg)');
			
			document.body.innerHTML = "";
			setTimeout(function(){update_screen(current_screen+1)},5000)
			break;
		case 4:
			$('html').css('background-image', 'url(images/BG4.jpg)');
			

			//	Consumption rgba(0, 0, 0,1)
			//  Grid rgba(128, 128, 128, 1)
			//  HVAC rgba(255, 208, 52,1)
			//  Lighting rgba(255, 76, 59,1)
			//  Power rgba(198, 200, 202,1)
			//  Battery Discharge rgba(0, 114, 187,1)
			//  Solar Generation rgba(50, 185, 45,1)
			
			var start_date = new Date(new Date().getTime() - 7 * 24 * 60 * 60 * 1000);
			start_date.setHours(0,0,0,0);
			var start_time = start_date.getTime();
			
			var end_date = new Date(new Date().getTime());
			end_date.setHours(0,0,0,0);
			var end_time = end_date.getTime();
			
			var show_inputs = [];
			show_inputs.push({outpost_name:'KWH_Day_Total_Export_Solar',graph_name:'Solar Generation',generation:true});
			
			var data_array = [];
			
			for (var i=0;i<show_inputs.length;i++) {
				data_array[show_inputs[i].outpost_name] = [];
			}

			var start_date = moment().subtract(7, "days");
			start_date.set({hour:0,minute:0,second:0,millisecond:0});
			var end_date = moment();
			end_date.set({hour:0,minute:0,second:0,millisecond:0});
			var period_days = end_date.diff(start_date, 'days');
			var temp_date = start_date;
			var data_labels = [];
			
			for (var i=0;i<period_days;i++) {
				temp_date.add(1,'days');
				for (var k=0;k<show_inputs.length;k++) {
					if (typeof data_array[show_inputs[k].outpost_name] == 'undefined') {
						data_array[show_inputs[k].outpost_name] = [];
					}
					if (Interval_Data[show_inputs[k].outpost_name].hasOwnProperty(temp_date.format("YYYY-MM-DDT00:00:00"))) {
						data_array[show_inputs[k].outpost_name].push(Interval_Data[show_inputs[k].outpost_name][temp_date.format("YYYY-MM-DDT00:00:00")]);
					} else {
						data_array[show_inputs[k].outpost_name].push(0);
					}
				}
			}
			
			temp_date = moment().subtract(7, "days");
			for (var i=0;i<period_days;i++) {
				temp_date.add(1,'days');
				data_labels.push(temp_date.format("YYYY-MM-DD"));
			}
			
			
			document.body.innerHTML = "";
			document.body.innerHTML +="<div id='main'>" +
			"<canvas  id='cvs' width='1450' height='430'>" +
			"[No canvas support]" +
			"</canvas>" +
			"</div>";
			
			function data_arrays_populated() {
				for (var i=0;i<show_inputs.length;i++) {
					if (data_array[show_inputs[i].outpost_name].length == 0 || typeof data_array[show_inputs[i].outpost_name] == 'undefined') {
						return false;
					}
				}
				return true;
			}
			

			if (data_arrays_populated()) {
				/* 
					calculating cars off road 
					based on emmissions factor 
					tc02/kwh = 0.00126
					
					tonnes CO2-e produced by av light vehicle per km source: http://www.greenvehicleguide.gov.au/pages/Information/VehicleEmissions
					tc02/km = 0.000188 
					
					Av kms per year travelled source: http://www.abs.gov.au/ausstats/abs@.nsf/mf/9208.0/
					13,800kms
				
				*/
				
				var tc02_saved_alltime = solar_total_alltime * 0.00126;
				var avg_car_kms_alltime = solar_days_active * (13800/365); 
				var tc02_avg_car_alltime = avg_car_kms_alltime * 0.000188;
				var cars_off_road = tc02_saved_alltime / tc02_avg_car_alltime;
				
				
				document.body.innerHTML +="<div id='cars_off_road'>" +
				cars_off_road.toFixed(2) +
				"</div>" +
				"<div id='since_install_text'>since installation</div>" +
				"<div id='solar_week'>this week's output<div class='num'>" + solar_total_week.toFixed(2) + " KWH</div></div>" +
				"<div id='solar_month'>this month's output <div class='num'>" + solar_total_month.toFixed(2) + " KWH</div></div>" +
				"<div id='solar_year'>this year's output <div class='num'>" + solar_total_year.toFixed(2) + " KWH</div></div>" + 
				"<div id='solar_all'>output since installation <div class='num'>" + solar_total_alltime.toFixed(2) + " KWH</div></div>";
				
			};

			
			var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
			var months = ['January','February','March','April','May','June','July','August','September','October','November','December'];
			
			var ctx = document.getElementById('cvs').getContext('2d');
			if (typeof window.myBarChart != 'undefined') {
				window.myBarChart.destroy();
			}
			window.myBarChart = new Chart(ctx, {
				type: 'bar',
				data: {
					labels: data_labels,
					datasets: [{					
						label: "Solar Generation",
						backgroundColor: 'rgba(50, 185, 45,1)',
						borderColor: 'rgba(50, 185, 45,1)',
						data: data_array["KWH_Day_Total_Export_Solar"]
					}]
				},
				options: {
					title:{
						display:true,
						fontSize: 20,
						text:'Beacon Solar Generation - ' + Site_Name						
//						text:Site_Name + ' KWH Profile past 7 days'
					},
					scales: {
						yAxes: [{
							display: true,
							scaleLabel: {
								display: true,
								labelString: 'KWH'
							},
				            ticks: {
								beginAtZero: true
							}
						}]
					}
				}				
			});
			setTimeout(function(){update_screen(current_screen+1)},15000);
			break;
		case 5:
			$('html').css('background-image', 'url(images/BG5.jpg)');
			document.body.innerHTML = "";
			
			setTimeout(function(){update_screen(1)},5000);
			break;

		case 6: /* KW 7 Day Profile, Grid , HVAC, Solar Gen*/
			$('html').css('background-image', 'url(images/BG4.jpg)');
//			current_screen++;

			//	Consumption rgba(0, 0, 0,1)
			//  Grid rgba(128, 128, 128, 1)
			//  HVAC rgba(255, 208, 52,1)
			//  Lighting rgba(255, 76, 59,1)
			//  Power rgba(198, 200, 202,1)
			//  Battery Discharge rgba(0, 114, 187,1)
			//  Solar Generation rgba(50, 185, 45,1)

			var start_date = new Date(new Date().getTime() - 7 * 24 * 60 * 60 * 1000);
			start_date.setHours(0,0,0,0);
			var start_time = start_date.getTime();
			
			var end_date = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
			end_date.setHours(0,0,0,0);
			var end_time = end_date.getTime();
			
			var show_inputs = [];
			show_inputs.push('KWH_Interval_Grid');
			show_inputs.push('KWH_Interval_HVAC');
			show_inputs.push('KWH_Interval_Export_Solar');
			
			var data_array = [];
			
			for (var i=0;i<show_inputs.length;i++) {
				data_array[show_inputs[i]] = [];
			}
			
			for (var i=0;i<show_inputs.length;i++) {
				for (var key in Interval_Data[show_inputs[i]]) {
					if (Interval_Data[show_inputs[i]].hasOwnProperty(key)) {
						var x_moment = moment(key,"YYYY-MM-DDTHH:mm:ss");
						if (x_moment.isValid()) {
							var start_date = moment().subtract(7, "days");
							start_date.set({hour:0,minute:0,second:0,millisecond:0});
							var end_date = moment().add(1, "days");
							end_date.set({hour:0,minute:0,second:0,millisecond:0});
							if (x_moment.isBetween(start_date,end_date)) {
								data_array[show_inputs[i]].push({x:x_moment,y:parseFloat(Interval_Data[show_inputs[i]][key]) * (3600/Interval_Data[show_inputs[i]].Log_Interval)});
							}
						}
					}
				}
				data_array[show_inputs[i]].sort(function (a, b) {
					return a.y - b.y;
				});
				
				if (typeof data_array[show_inputs[i]][0] != 'undefined') {
					data_array[show_inputs[i]][0]["label"] = "min";
					data_array[show_inputs[i]][data_array[show_inputs[i]].length-1]["label"] = "max";
				}
				data_array[show_inputs[i]].sort(function (a, b) {
					return a.x - b.x;
				});
			}
			
			document.body.innerHTML = "";
			document.body.innerHTML +="<div id='main'>" +
			"<canvas  id='cvs' width='1450' height='430'>" +
			"[No canvas support]" +
			"</canvas>" +
			"</div>";
			
			if (data_array["KWH_Interval_Export_Solar"].length > 1 && data_array["KWH_Interval_Grid"].length > 1 && data_array["KWH_Interval_HVAC"].length > 1) {
				/* 
					calculating cars off road 
					based on emmissions factor 
					tc02/kwh = 0.00109
					
					tonnes CO2-e produced by av light vehicle per km source: http://www.greenvehicleguide.gov.au/pages/Information/VehicleEmissions
					tc02/km = 0.000188 
					
					Av kms per year travelled source: http://www.abs.gov.au/ausstats/abs@.nsf/mf/9208.0/
					13,800kms
				
				*/
				
				var solar_total = data_array["KWH_Interval_Export_Solar"].reduce(get_point_sum,0);
				var tc02_saved_7days = solar_total * 0.00109;
				
				var avg_car_kms_7days = 7 * (13800/365);
				
				var tc02_avg_car_7days = avg_car_kms_7days * 0.000188;
				
				var cars_off_road = tc02_saved_7days / tc02_avg_car_7days;
				
				var total_gen = data_array["KWH_Interval_Export_Solar"].reduce(get_point_sum,0);
				var total_hvac = data_array["KWH_Interval_HVAC"].reduce(get_point_sum,0);
				var total_grid = data_array["KWH_Interval_Grid"].reduce(get_point_sum,0); 
				
				document.body.innerHTML +="<div id='cars_off_road'>" +
				cars_off_road.toFixed(2) +
				"</div>" +
				"<div id='solar'>" +
				"	<div class='title'>" +
				"	<h1>this week's output</h1>" +
				" </div> " +
				" <div class='max'>" +         
				" <h2>Max <div class='num'>" + Math.max.apply(Math, data_array["KWH_Interval_Export_Solar"].map(function(o) { return o.y; })).toFixed(2) + " KW</div></h2> " +
				" </div> " +
				" <div class='min'>" +
				"<h3>Average <div class='num'>" + (total_gen / data_array["KWH_Interval_Export_Solar"].length).toFixed(2) + " KW</div></h3>" +
				" </div>" +
				"</div>" +
				"<div id='grid'>" +
				"<div class='title'>" +
				"<h1>Grid Consumption</h1>" +
				"</div>" +
				"<div class='max'>" +
				"<h2>Max <div class='num'>" + Math.max.apply(Math, data_array["KWH_Interval_Grid"].map(function(o) { return o.y; })).toFixed(2) + " KW</div></h2>" +
				"</div>" +
				"<div class='min'>" +
				"<h3>Average <div class='num'>" + (total_grid / data_array["KWH_Interval_HVAC"].length).toFixed(2) + " KW</div></h3>" + 
				"</div>"+
				"</div>"+
				"<div id='hvac'>"+
				"<div class='title'>"+
				"<h1>HVAC Consumption</h1>"+
				"</div>"+
				"<div class='max'>"+
				"<h2>Max <div class='num'>" + Math.max.apply(Math, data_array["KWH_Interval_HVAC"].map(function(o) { return o.y; })).toFixed(2) + " KW</div></h2>"+
				"</div>"+
				"<div class='min'>"+
				"<h3>Average <div class='num'>" + (total_hvac / data_array["KWH_Interval_Grid"].length).toFixed(2) + " KW</div></h3>"+
				"</div>"+
				"</div>";
			};

			
			var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
			var months = ['January','February','March','April','May','June','July','August','September','October','November','December'];
			
			var ctx = document.getElementById('cvs').getContext('2d');
			if (typeof window.myLineChart != 'undefined') {
				window.myLineChart.destroy();
			}
			window.myLineChart = new Chart(ctx, {
				type: 'line',
				data: {
					datasets: [{
						label: "Grid",
						backgroundColor: 'rgba(0, 0, 0,1)',
						borderColor: 'rgba(0, 0, 0,1)',
						data: data_array["KWH_Interval_Grid"],
						fill: false
					}, {
						label: "HVAC",
						backgroundColor: 'rgba(0, 114, 187,1',
						borderColor: 'rgba(0, 114, 187,1',
						data: data_array["KWH_Interval_HVAC"],
						fill: false
					}, {					
						label: "Solar Generation",
						backgroundColor: 'rgba(50, 185, 45,1)',
						borderColor: 'rgba(50, 185, 45,1)',
						data: data_array["KWH_Interval_Export_Solar"],
						fill: false
					}]
				},
				options: {
					elements: {
						point:{
							radius: 0
					}},
					animation:false,
					responsive: true,
					title:{
						display:true,
						fontSize: 20,
						text:'Beacon Solar Generation - ' + Site_Name
//						text:Site_Name + ' KW Profile past 7 days'
					},
					tooltips: {
						mode: 'index',
						intersect: false,
					},
					hover: {
						mode: 'nearest',
						intersect: true
					},
					scales: {
						xAxes: [{
							type: "time",
							display: true,
						}],
						yAxes: [{
							display: true,
							scaleLabel: {
								display: true,
								labelString: 'KW'
							}
						}]
					}
				}
			});
			current_screen++;
			setTimeout(function(){update_screen()},15000);
			break;
		default:
		   setTimeout(function(){update_screen()},5000);
		}
	}

	</script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script>
		$(document).ready(function ()
		{
			
			
			
			
			get_Site_Data(['KWH_Interval_Export_Solar'],'Today');
			get_Site_Data(['KWH_Day_Total_Export_Solar'],'All Time');
			
			setInterval( function() { get_Site_Data(['KWH_Interval_Export_Solar'],'Today'); }, 600000 );
			setInterval( function() { get_Site_Data(['KWH_Day_Total_Export_Solar'],'All Time'); }, 21600000 );
			update_screen(1);
		});	
	
	</script>

	
	<body style="padding:0; margin:0px;">
	</body>
</html>



