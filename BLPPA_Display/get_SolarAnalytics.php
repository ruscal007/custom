<?php
	ini_set('max_execution_time', 1500);
	ini_set('memory_limit', '1024M');
	header("Content-Type: application/json", true);
	$solarAnalytics_id = $_REQUEST['SolarAnalyticsID'];
	$type = $_REQUEST['type']; // set to kw or kwh
	$dateFrom = strtotime($_REQUEST['dateFrom']); // should be yyyy-mm-dd 00:00:00 format
	$dateTo = strtotime($_REQUEST['dateTo']); // should be yyyy-mm-dd 00:00:00 format
	$deviceID = $_REQUEST['D_ID'];
	$siteID = $_REQUEST['S_ID'];
	
// set feed URL

function get_token() {
	$filePath = getcwd().DIRECTORY_SEPARATOR."sa_token".DIRECTORY_SEPARATOR."token.txt";
//echo $filePath;

	if (file_exists($filePath)){
		$objData = file_get_contents($filePath);
		$obj = unserialize($objData);           
		if (!empty($obj)){
			if (strtotime($obj->expires) > time()){
//				echo"token from file";
				return $obj;
			}
		} 
	}

	$url = "https://portal.solaranalytics.com.au/api/v3/token";

	$ch = curl_init();
	curl_setopt($ch,CURLOPT_CONNECTTIMEOUT, 4);
	curl_setopt($ch, CURLOPT_URL,$url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
	curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	curl_setopt($ch, CURLOPT_USERPWD, "itadmin@carbonetix.com.au:k7S0i3g2015");
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	$request_time = time();
	$json = curl_exec($ch);
	curl_close($ch);
	if(!$json) {
		return false;
	} else {
//		echo "got token i guess";
		$token = json_decode($json);
		
		$objData = serialize($token);
		$filePath = getcwd().DIRECTORY_SEPARATOR."sa_token".DIRECTORY_SEPARATOR."token.txt";
		if (is_writable($filePath)) {
			$fp = fopen($filePath, "w"); 
			fwrite($fp, $objData); 
			fclose($fp);
		}
		else {
//			echo "cant write token to file";
		}
//		echo "Token from server";
		return json_decode($json);
	}
}


$result = get_token();

//print_r($result);
if (is_object($result)) {
//		print ("your token is" . $result->token . "\n");
		
	switch ($type) {
		case 'store_sa':
			$url = 'https://portal.solaranalytics.com.au/api/v2/site_list/';
			$http_data = http_build_query(array(
			"ww_devices"  => "true",
			"circuits"  => "true",
			"owner_details" => "true",
			"ppa_details"  => "true"
			));
		break;
		case 'site_data':
			$url = 'https://portal.solaranalytics.com.au/api/v2/site_list/';
			$url = $url . $solarAnalytics_id . "?";
			$http_data = http_build_query(array(
			"ww_devices"  => "false",
			"circuits"  => "false",
			"owner_details" => "false",
			"ppa_details"  => "false"
			));
		break;
		case 'device_data':
			$url = 'https://portal.solaranalytics.com.au/api/v2/live_data/';
			$url = $url . $siteID . "?";
			$http_data = http_build_query(array(
			"device" => $deviceID,
			"tstamp"  => date('Y-m-d H:i:s',$dateFrom),
			"force_polarity"  => "true",
			"all" => "true"
	//		"utchour" => date('Y-m-d H:i:s',$dateTo)
			));
			break;
		case 'device_list':
			$url = 'https://portal.solaranalytics.com.au/api/v2/site_list/';
			$url = $url . $solarAnalytics_id . "?";
			$http_data = http_build_query(array(
			"ww_devices"  => "true",
			"circuits"  => "true",
			"owner_details" => "true",
			"ppa_details"  => "true"
			));
			break;
		case 'kw':
			$url = "https://portal.solaranalytics.com.au/api/v2/live_site_data/";
			$url = $url . $siteID . "?";
			$http_data = http_build_query(array(
			"utchour"  => date('Y-m-d H:i:s',$dateFrom),
			"last_sync"  => "false",
			"last_six" => "false",
			"last_hour"  => "true",
			"battery_data"  => "false"
			));
			break;
		case 'kwh_minute':
			$url = "https://portal.solaranalytics.com.au/api/v2/site_data/";
			$url = $url . $siteID . "?";
			$http_data = http_build_query(array(
			"tstart"  => date('Ymd',$dateFrom),
			"tend"  => date('Ymd',$dateTo),
			"gran" => 'minute',  //Possible values:  minute , hour , day , month , year .
			"raw"  => 'true',  //true or false, will return highest granulatrity if true
			"trunc"  => 'false', // set to false to return floats rather than truncated ints
			"all" => 'true'		
			));
			break;
		case 'kwh_hourly':
			$url = "https://portal.solaranalytics.com.au/api/v2/site_data/";
			$url = $url . $siteID . "?";
			$http_data = http_build_query(array(
			"tstart"  => date('Ymd',$dateFrom),
			"tend"  => date('Ymd',$dateTo),
			"gran" => 'hour',  //Possible values:  minute , hour , day , month , year .
			"raw"  => 'false',  //true or false, will return highest granulatrity if true
			"trunc"  => 'false', // set to false to return floats rather than truncated ints
			"all" => 'true'		
			));
			break;
		case 'kwh_daily':
			$url = "https://portal.solaranalytics.com.au/api/v2/site_data/";
			$url = $url . $siteID . "?";
			$http_data = http_build_query(array(
			"tstart"  => date('Ymd',$dateFrom),
			"tend"  => date('Ymd',$dateTo),
			"gran" => 'day',  //Possible values:  minute , hour , day , month , year .
			"raw"  => 'false',  //true or false, will return highest granulatrity if true
			"trunc"  => 'false', // set to false to return floats rather than truncated ints
			"all" => 'true'
			));
			break;
		case 'kwh_monthly':
			$url = "https://portal.solaranalytics.com.au/api/v2/site_data/";
			$url = $url . $siteID . "?";
			$http_data = http_build_query(array(
			"tstart"  => date('Ymd',$dateFrom),
			"tend"  => date('Ymd',$dateTo),
			"gran" => 'month',  //Possible values:  minute , hour , day , month , year .
			"raw"  => 'false',  //true or false, will return highest granulatrity if true
			"trunc"  => 'false', // set to false to return floats rather than truncated ints
			"all" => 'true'		
			));
			break;
	}



	//
	//$url .= "userName=BLPPA_Display&password=BLPPA_Display&dateFrom=". $dateFrom ."&dateTo=". $dateTo ."&outpostID=".$outpost_id."&format=xml";
	//echo $url . "\n";
	//$url=urlencode($url);







	switch ($type) {
		case 'store_sa':
			
			while (true) {
			
			
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , "Authorization: Bearer " . $result->token ));
			curl_setopt($ch,CURLOPT_URL,$url . $http_data);
			curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
			curl_setopt($ch,CURLOPT_CONNECTTIMEOUT, 4);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			$json = curl_exec($ch);
			if(!$json) {
				echo curl_error($ch);
			}
			curl_close($ch);
		
		
			$data = json_decode($json);
			
			$data = $data->data;
			$data = $data->sites[0];
			print_r("\n");
			var_dump($data);
			print_r("\n");
			
			
			$localconnect = "localhost";
			$database = "sa_users";
			$username = "sauser";
			$password = "c41p0su";
		
			$link = mysql_connect($localconnect,$username, $password);
			if (! $link)
			die("connection error");
			else
				mysql_select_db($database);
		
			$sql = "INSERT INTO sites ("
				."id, "
				."site_name, "		
				."sa_site_name, "
				."site_classification, "
				."st_address, "
				."state, "
				."subs_prod, "
				."timezone, "
				."postcode, "
				."ppa_next_invoice_start, "
				."ppa_product_type, "
				."ppa_rate, "
				."ppa_site, "
				."lng, "
				."lat, "
				."city, "
				."country, "
				."d_type, "
				."dc_power, "
				."account_number, "
				."active, "
				."billing_portal, "
				."owner_email, "
				."owner_given_name, "
				."owner_surname) "
				."VALUES ("
				. $data->site_id . ", "
				. "'" . $data->site_name . "', "
				. "'" . $data->sa_site_name . "', "
				. "'" . $data->site_classification . "', "
				. "'" . $data->st_address . "', "
				. "'" . $data->state . "', "
				. "'" . $data->subs_prod . "', "
				. "'" . $data->timezone . "', "
				. "'" . $data->postcode . "', "
				. "'" . $data->ppa_next_invoice_start . "', "
				. "'" . $data->ppa_product_type . "', "
				. "'" . $data->ppa_rate . "', "
				. "'" . $data->ppa_site . "', "
				. "'" . $data->lng_lat[0] . "', "
				. "'" . $data->lng_lat[1] . "', "
				. "'" . $data->city . "', "
				. "'" . $data->country . "', "
				. "'" . $data->d_type . "', "
				. "'" . $data->dc_power . "', "
				. "'" . $data->account_number . "', "
				. "'" . $data->active . "', "
				. "'" . $data->billing_portal . "', "
				. "'" . $data->owner_info->email . "', "
				. "'" . $data->owner_info->given_name . "', "
				. "'" . $data->owner_info->surname . "')";
		
		//print($sql . "\n");		
				$query_type=mysql_query($sql);
				
				foreach ($data->circuits as $circuit)
				{
					$sql = "INSERT INTO circuits ("
						."id, "
						."monitor_type, "
						."fk_site_id) "
						."VALUES ("
						. $circuit->circuit_id . ", "
						. "'" . $circuit->monitor_type . "', "
						. "'" . $data->site_id . "')";
					$query_type=mysql_query($sql);
		//			print $sql . "\n";
				}
				
				foreach ($data->ww_serials as $ww_serial)
				{
					$sql = "INSERT INTO ww_serials ("
						."serial, "
						."fk_site_id) "
						."VALUES ('"
						. $ww_serial . "', "
						. "'" . $data->site_id . "')";
					$query_type=mysql_query($sql);
		//			print $sql . "\n";	
				}
				print ($solarAnalytics_id . "\n");
		
		
		
				if ($start_time + $result->duration <= time()) {
					$result = get_token();
				} else {
					$solarAnalytics_id++;
				}
			}
		break;
		case 'site_data':
			$ch = curl_init();	
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , "Authorization: Bearer " . $result->token ));
			curl_setopt($ch,CURLOPT_URL,$url . $http_data);
			curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
			curl_setopt($ch,CURLOPT_CONNECTTIMEOUT, 4);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			$json = curl_exec($ch);
			

			$json = curl_exec($ch);

			if(!$json) {
				echo curl_error($ch);
			}
			curl_close($ch);
			
			$json = json_decode($json);
			
			
			
			foreach ($json->data->sites as $site) {
				if ($site->site_id == $siteID) {
					print_r(json_encode($site));
				}
			}
			
		
		break;
		default:
		
		
			$ch = curl_init();	
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , "Authorization: Bearer " . $result->token ));
			curl_setopt($ch,CURLOPT_URL,$url . $http_data);
			curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
			curl_setopt($ch,CURLOPT_CONNECTTIMEOUT, 4);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			$json = curl_exec($ch);
			

			$json = curl_exec($ch);
			if(!$json) {
				echo curl_error($ch);
			}
			curl_close($ch);
			
			print_r(json_encode($json));
			
			

	}


	//print($url . $solarAnalytics_id . "?" . $data . "\n");
	//print_r(json_decode($json));


	
}





?>



