<?php
//  
//	header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
//	header("Cache-Control: post-check=0, pre-check=0", false);
//	header("Pragma: no-cache");
// http://smartportal.com.au/BLPPA_Display/blppa_display.php?oID=op30862	
	
	$outpost_id = "op34987";

?>
	<!DOCTYPE html
		PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
		"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
	<title>BLPPA Display</title>
		<style type="text/css">
			html {
			  background-size:100%;
			  background-repeat:no-repeat;
			  -webkit-background-size: cover;
			  -moz-background-size: cover;
			  -o-background-size: cover;
			  
			}

			body, p{
				font-family:Calibri, verdana;
				font-size:16px;
			}
			
			
			#main {
				position: absolute;
				left: 150px;
				top: 160px;
			}
			
			.infogfx {
				display: flex;
				align-items: center;
			}
			</style>
	</head>
	<script src="./includes/moment-with-locales.min.js"></script>	
	<script src="./includes/Chart.js"></script>

	<script>
	
	var Site_Name;

	var current_screen;
	
	current_screen = 1;
	
	var Interval_Data = [];
	var Monthly_Data = [];
	var Daily_Data = [];
	

	var KWH_day_total = [];

	var inputs = [];
	
	inputs.push('KWH_Interval_Battery');
	inputs.push('KWH_Interval_Battery_Export');
	inputs.push('KWH_Interval_Grid');
	inputs.push('KWH_Interval_Grid_Export');
	inputs.push('KWH_Interval_HVAC');
	inputs.push('KWH_Interval_Lighting');
	inputs.push('KWH_Interval_Power');
	inputs.push('KWH_Interval_Solar');
	inputs.push('KWH_Interval_Consumption');
	
	for (var i=0;i<inputs.length;i++) {
		Interval_Data[inputs[i]] = [];
		KWH_day_total[inputs[i]] = [];
	}
	
	function get_Op_Interval_Records(input,start_time,end_time) {
		var start_date = new Date(start_time);
		var end_date = new Date(end_time);
		
		var dateFrom =  start_date.getDate() + "/" + (start_date.getMonth() + 1) + "/" + start_date.getFullYear() + "+00:00:00" ;
		var dateTo =  end_date.getDate() + "/" + (end_date.getMonth() + 1) + "/" + end_date.getFullYear() + "+00:00:00" ;
		url = "get_Outpost.php?inputName=" + input + "&dateFrom=" + dateFrom +"&dateTo=" + dateTo +"&outpostID=<?=$outpost_id?>" + "&format=xml";
		
		$.ajax({
			type: "GET",
			url: url,
			dataType: "xml",
			success: function(xml) {
					Site_Name = $(xml).find('opdata\\:site, site').children('name').text();
					Last_Upload = $(xml).find('opdata\\:logger, logger').children('last_telemetry').text();
					Log_Interval = $(xml).find('opdata\\:input, input').children('logInterval').text();
					Log_Interval = parseFloat(Log_Interval);

					Interval_Data[input].Log_Interval = Log_Interval;
					
					var dateChanged = true;
					var oldDate;
					
					$(xml).find('opdata\\:record, record').each(function(){
						if (typeof oldDate != 'undefined') {
							if (oldDate != $(this).find('date').text().substr(0, $(this).find('date').text().indexOf('T'))) {
								dateChanged = true;
							} else {
								dateChanged = false;
							}
						}

						Interval_Data[input][$(this).find('date').text()] = parseFloat($(this).find('value').text());
						if (typeof KWH_day_total[input][$(this).find('date').text().substr(0, $(this).find('date').text().indexOf('T'))] != 'undefined' && !dateChanged) {
							KWH_day_total[input][$(this).find('date').text().substr(0, $(this).find('date').text().indexOf('T'))] += parseFloat($(this).find('value').text());
						} else {
							KWH_day_total[input][$(this).find('date').text().substr(0, $(this).find('date').text().indexOf('T'))] = parseFloat($(this).find('value').text());
						}
						
						oldDate = $(this).find('date').text().substr(0, $(this).find('date').text().indexOf('T'));
					});
					

			}
		}); 
	}
	
	function get_Site_Data(input_list,period,repeat_time) {
		switch(period) {
		case 'Today':
			var start_date = new Date(new Date().getTime());
			start_date.setHours(0,0,0,0);
			var start_time = start_date.getTime();
			
			var end_date = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
			end_date.setHours(0,0,0,0);
			var end_time = end_date.getTime();
		
		break;
		case 'Past 7 Days':
			var start_date = new Date(new Date().getTime() - 6 * 24 * 60 * 60 * 1000);
			start_date.setHours(0,0,0,0);
			var start_time = start_date.getTime();
			
			var end_date = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
			end_date.setHours(0,0,0,0);
			var end_time = end_date.getTime();

		break;
		default:
		}

		for (var i=0;i<input_list.length;i++) {
			get_Op_Interval_Records(input_list[i],start_time,end_time);
		}
		
		if (repeat_time>0) {
			setTimeout(function(){get_Site_Data(input_list,period,repeat_time)},repeat_time);
		}
	}	

	function getSum(total, num) {
		return total + num;
	}

	function update_screen() {

		var mainHTML;
		mainHTML = "";
		
		switch(current_screen) {
		case 1:
//  Bar chart showing KWH produced today for the following circuits
//	Consumption rgba(0, 0, 0,1)
//  Grid rgba(128, 128, 128, 1)
//  HVAC rgba(255, 208, 52,1)
//  Lighting rgba(255, 76, 59,1)
//  Power rgba(198, 200, 202,1)
//  Battery Discharge rgba(0, 114, 187,1)
//  Solar Generation rgba(50, 185, 45,1)


			var start_date = new Date(new Date().getTime());
			start_date.setHours(0,0,0,0);
			var start_time = start_date.getTime();
			
			var end_date = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
			end_date.setHours(0,0,0,0);
			var end_time = end_date.getTime();
			
			var show_inputs = [];
			show_inputs.push('KWH_Interval_Power');
			show_inputs.push('KWH_Interval_Lighting');
			show_inputs.push('KWH_Interval_HVAC');
			
			$('html').css('background-image', 'url(images/BG1.jpg)');
			
			document.body.innerHTML = "";
		    
			document.body.innerHTML +="<div id='main'>" +
			"<canvas  id='cvs' width='1650' height='720'>" +
			"[No canvas support]" +
			"</canvas>" +
			"</div>";
			
			var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
			var months = ['January','February','March','April','May','June','July','August','September','October','November','December'];

			
			var ctx = document.getElementById('cvs').getContext('2d');
			if (typeof window.myBarChart != 'undefined') {
				window.myBarChart.destroy();
			}

			var data_array = [];
			data_array.push(KWH_day_total["KWH_Interval_Power"][moment().format('YYYY-MM-DD')]);
			data_array.push(KWH_day_total["KWH_Interval_Lighting"][moment().format('YYYY-MM-DD')]);
			data_array.push(KWH_day_total["KWH_Interval_HVAC"][moment().format('YYYY-MM-DD')]);

			window.myBarChart = new Chart(ctx, {
				type: 'bar',
				data: {
					labels: ["Power","Lighting","HVAC"],
					datasets: [{
						backgroundColor: ['rgba(150,150,150,1)','rgba(125,125,125,1)','rgba(100,100,100,1)'],
						borderColor: 'rgba(0, 0, 0,1)',
						data: data_array
					}]
				},
				options: {
					legend: {
						display: false
					},
					elements: {
						point:{
							radius: 0
						}
					},
					animation:false,
					responsive: true,
					title:{
						display:true,
						fontSize: 20,
						text:'South East Annex Todays Energy'
					},
					tooltips: {
						mode: 'index',
						intersect: false,
					},
					hover: {
						mode: 'nearest',
						intersect: true
					},
					scales: {
						yAxes: [{
							display: true,
							scaleLabel: {
								display: true,
								labelString: 'KWH'
							}
						}]
					}				
				},
			});
	
			setTimeout(function(){update_screen()},15000);
			current_screen++;
			break;
		case 2:
//  Bar chart showing KWH produced today for the following circuits
//	Consumption rgba(0, 0, 0,1)
//  Grid rgba(128, 128, 128, 1)
//  HVAC rgba(255, 208, 52,1)
//  Lighting rgba(255, 76, 59,1)
//  Power rgba(198, 200, 202,1)
//  Battery Discharge rgba(0, 114, 187,1)
//  Solar Generation rgba(50, 185, 45,1)


			var start_date = new Date(new Date().getTime());
			start_date.setHours(0,0,0,0);
			var start_time = start_date.getTime();
			
			var end_date = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
			end_date.setHours(0,0,0,0);
			var end_time = end_date.getTime();
			
			var show_inputs = [];
			show_inputs.push('KWH_Interval_Grid');
			show_inputs.push('KWH_Interval_Battery_Export');
			show_inputs.push('KWH_Interval_Battery');
			show_inputs.push('KWH_Interval_Solar');
			
			$('html').css('background-image', 'url(images/BG1.jpg)');
			
			document.body.innerHTML = "";
		    
			document.body.innerHTML +="<div id='main'>" +
			"<canvas  id='cvs' width='1650' height='720'>" +
			"[No canvas support]" +
			"</canvas>" +
			"</div>";
			
			var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
			var months = ['January','February','March','April','May','June','July','August','September','October','November','December'];

			
			var ctx = document.getElementById('cvs').getContext('2d');
			if (typeof window.myBarChart != 'undefined') {
				window.myBarChart.destroy();
			}

			var data_array = [];
			data_array.push(KWH_day_total["KWH_Interval_Grid"][moment().format('YYYY-MM-DD')]);
			data_array.push(KWH_day_total["KWH_Interval_Battery_Export"][moment().format('YYYY-MM-DD')]);
			data_array.push(KWH_day_total["KWH_Interval_Battery"][moment().format('YYYY-MM-DD')]);			
			data_array.push(KWH_day_total["KWH_Interval_Solar"][moment().format('YYYY-MM-DD')]);

			window.myBarChart = new Chart(ctx, {
				type: 'bar',
				data: {
					labels: ["Grid","Battery Discharge","Battery Charge","Solar"],
					datasets: [{
						backgroundColor: ['rgba(50,50,50,1)','rgba(150,150,150,1)','rgba(0, 114, 187,1','rgba(50, 185, 45,1)'],
						borderColor: 'rgba(0, 0, 0,1)',
						data: data_array
					}]
				},
				options: {
					legend: {
						display: false
					},
					elements: {
						point:{
							radius: 0
						}
					},
					animation:false,
					responsive: true,
					title:{
						display:true,
						fontSize: 20,
						text:'South East Annex Todays Energy'
					},
					tooltips: {
						mode: 'index',
						intersect: false,
					},
					hover: {
						mode: 'nearest',
						intersect: true
					},
					scales: {
						yAxes: [{
							display: true,
							scaleLabel: {
								display: true,
								labelString: 'KWH'
							}
						}]
					}				
				},
			});
	
			setTimeout(function(){update_screen()},15000);
			current_screen++;
			break;



		case 3:
//  line chart showing KW profile today for the following circuits
//	Consumption rgba(0, 0, 0,1)
//  Grid rgba(128, 128, 128, 1)
//  HVAC rgba(255, 208, 52,1)
//  Lighting rgba(255, 76, 59,1)
//  Power rgba(198, 200, 202,1)
//  Battery Discharge rgba(0, 114, 187,1)
//  Solar Generation rgba(50, 185, 45,1)
			var start_date = new Date(new Date().getTime());
			start_date.setHours(0,0,0,0);
			var start_time = start_date.getTime();
			
			var end_date = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
			end_date.setHours(0,0,0,0);
			var end_time = end_date.getTime();
			
			var tempDate;
			
			var show_inputs = [];
			show_inputs.push('KWH_Interval_Consumption');
			show_inputs.push('KWH_Interval_Battery_Export');
			show_inputs.push('KWH_Interval_Solar');
			
			
			var data_array = [];
			
			for (var i=0;i<show_inputs.length;i++) {
				data_array[show_inputs[i]] = [];
			}
			
			for (var i=0;i<show_inputs.length;i++) {

				for (var key in Interval_Data[show_inputs[i]]) {
					if (Interval_Data[show_inputs[i]].hasOwnProperty(key)) {
						var x_moment = moment(key,"YYYY-MM-DDTHH:mm:ss");
						if (x_moment.isValid()) {
							data_array[show_inputs[i]].push({x:x_moment,y:parseFloat(Interval_Data[show_inputs[i]][key]) * (3600/Interval_Data[show_inputs[i]].Log_Interval)});
						}
					}
				}

				tempDate = new Date(start_time);
					do {
						 if (typeof Interval_Data[show_inputs[i]][tempDate.getTime()] != 'undefined') {
							data_array[show_inputs[i]].push({x:x_moment,y:parseFloat(Interval_Data[show_inputs[i]][tempDate.getTime()]) * (3600/Interval_Data[show_inputs[i]].Log_Interval)});	
						 }
						tempDate.setSeconds ( tempDate.getSeconds() + Interval_Data[show_inputs[i]].Log_Interval );
					}
					while (tempDate.getTime() < end_time);
			}
			
			
			$('html').css('background-image', 'url(images/BG1.jpg)');
			
			document.body.innerHTML = "";
		    
			document.body.innerHTML +="<div id='main'>" +
			"<canvas  id='cvs' width='1550' height='720'>" +
			"[No canvas support]" +
			"</canvas>" +
			"</div>";
			
			var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];

			var months = ['January','February','March','April','May','June','July','August','September','October','November','December'];

			
		var ctx = document.getElementById('cvs').getContext('2d');
		if (typeof window.myLineChart != 'undefined') {
			window.myLineChart.destroy();
		}
		window.myLineChart = new Chart(ctx, {
            type: 'line',
            data: {
                datasets: [{
                    label: "Consumption - HVAC + Power + Lighting",
                    backgroundColor: 'rgba(0, 0, 0,1)',
                    borderColor: 'rgba(0, 0, 0,1)',
                    data: data_array["KWH_Interval_Consumption"],
                    fill: false
                }, {
                    label: "Battery Discharge",
                    backgroundColor: 'rgba(0, 114, 187,1',
                    borderColor: 'rgba(0, 114, 187,1',
                    data: data_array["KWH_Interval_Battery_Export"],
                    fill: false
				}, {					
                    label: "Solar Generation",
                    backgroundColor: 'rgba(50, 185, 45,1)',
                    borderColor: 'rgba(50, 185, 45,1)',
                    data: data_array["KWH_Interval_Solar"],
                    fill: false
                }]
            },
            options: {
                elements: {
                    point:{
                        radius: 0
				}},
				animation:false,
                responsive: true,
                title:{
                    display:true,
					fontSize: 20,					
                    text:'South East Annex Todays Energy'
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
						type: "time",
                        display: true,
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'KW'
                        }
                    }]
                }
            }
        });

			setTimeout(function(){update_screen()},15000);
			current_screen++;
			break;
		case 4:
			var start_date = new Date(new Date().getTime());
			start_date.setHours(0,0,0,0);
			var start_time = start_date.getTime();
			
			var end_date = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
			end_date.setHours(0,0,0,0);
			var end_time = end_date.getTime();
			
			$('html').css('background-image', 'url(images/BG1.jpg)');
  

			var Total_Consumption = KWH_day_total["KWH_Interval_Battery_Export"][moment().format('YYYY-MM-DD')] + KWH_day_total["KWH_Interval_Grid"][moment().format('YYYY-MM-DD')] + KWH_day_total["KWH_Interval_Solar"][moment().format('YYYY-MM-DD')] - KWH_day_total["KWH_Interval_Grid_Export"][moment().format('YYYY-MM-DD')];
			var Percentage_from_Grid = (KWH_day_total["KWH_Interval_Grid"][moment().format('YYYY-MM-DD')] / Total_Consumption) * 100;
			var Percentage_from_SB = ((KWH_day_total["KWH_Interval_Battery_Export"][moment().format('YYYY-MM-DD')] + KWH_day_total["KWH_Interval_Solar"][moment().format('YYYY-MM-DD')] - KWH_day_total["KWH_Interval_Grid_Export"][moment().format('YYYY-MM-DD')]) / Total_Consumption)*100;

			
			document.body.innerHTML = "<div id='main'>" +
				"<div>" +
				"<p style='font-size:50px'>Total Energy Consumed Today " + Math.round(Total_Consumption*100)/100 + " KWH</p>" +
				"<div class='infogfx'><p style='font-size:125px'>" + Math.round(Percentage_from_SB*100)/100 + " % </p><img src='images/iconsolar.jpg' height ='250px'><img src='images/iconbat.jpg' height ='250px'>" + 
				"<div class='infogfx'><p style='font-size:125px'>" + Math.round(Percentage_from_Grid*100)/100 + "% </p><img src='images/icongrid.jpg' height ='250px'>" + 
				"</div></div>"
			"<a href='https://www.freepik.com/free-vector/energy-icons-collection_1035782.htm'>Designed by Freepik</a>" +
			"</div>";
			
			
			setTimeout(function(){update_screen()},15000);

			current_screen = 1;

			
			break;
		case 5: /* KW 7 Day Profile, Grid , HVAC, Solar Gen*/
			$('html').css('background-image', 'url(images/BG4.jpg)');
			current_screen++;
			
			var Grid_data = [];
			var HVAC_data = [];
			var Solar_data = [];	
			
			var day_totals =[];
			
			var nextDay;
			var curDay;
			
			var SolarDayTotal;
			var GridDayTotal;
			var HVACDayTotal;
		
			SolarDayTotal = 0;
			GridDayTotal = 0;
			HVACDayTotal = 0;
			
			tempDate = seven_day_start_date;
		
			do {
				curDay = tempDate.getDate();
				if (typeof Interval_Data['KWH_Interval_Solar'][tempDate.getTime()] != 'undefined') {
					Solar_data.push(parseFloat(Interval_Data['KWH_Interval_Solar'][tempDate.getTime()]));
					SolarDayTotal += parseFloat(Interval_Data['KWH_Interval_Solar'][tempDate.getTime()]);
				} else {
					Solar_data.push(0);
				}
				if (typeof Interval_Data['KWH_Interval_Grid'][tempDate.getTime()] != 'undefined') {
					Grid_data.push(parseFloat(Interval_Data['KWH_Interval_Grid'][tempDate.getTime()]));
					GridDayTotal += parseFloat(Interval_Data['KWH_Interval_Grid'][tempDate.getTime()]);
				} else {
					Grid_data.push(0);
				}
				if (typeof Interval_Data['KWH_Interval_HVAC'][tempDate.getTime()] != 'undefined') {
					HVAC_data.push(parseFloat(Interval_Data['KWH_Interval_HVAC'][tempDate.getTime()]));
					HVACDayTotal += parseFloat(Interval_Data['KWH_Interval_HVAC'][tempDate.getTime()]);
				} else {
					HVAC_data.push(0);
				}
				tempDate.setHours ( tempDate.getHours() + 1 );
				nextDay = tempDate.getDate();
				
				if (nextDay!=curDay) {
					day_totals.push([SolarDayTotal,Grid_data,HVAC_data])
					SolarDayTotal = 0;
					GridDayTotal = 0;
					HVACDayTotal = 0;
				}
			}
			while (tempDate.getTime() <= seven_day_end_date.getTime() );
	
			document.body.innerHTML = "";

			document.body.innerHTML +="<div id='title'>" +
			"<h1>" + Site_Name + " kW Profile Past 7 Days</h1>" +
			"</div>" +
			"<div id='main'>" +
			"<canvas  id='cvs' width='1450' height='430'>" +
			"[No canvas support]" +
			"</canvas>" +
			"</div>" +
			"<div id='solar'>" +
			"	<div class='title'>" +
			"	<h1>Solar Production</h1>" +
			" </div> " +
			" <div class='max'>" +
			" <h2>Max <div class='num'>" + Math.max(...Solar_data) + " KW</div></h2> " +
			" </div> " +
			" <div class='min'>" +
			"<h3>Total <div class='num'>" + Solar_data.reduce(getSum).toFixed(2) + " KWH</div></h3>" +
			" </div>" +
			"</div>" +
			"<div id='grid'>" +
			"<div class='title'>" +
			"<h1>Grid Consumption</h1>" +
			"</div>" +
			"<div class='max'>" +
			"<h2>Max <div class='num'>" + Math.max(...Grid_data) + " KW</div></h2>" +
			"</div>" +
			"<div class='min'>" +
			"<h3>Total <div class='num'>" + Grid_data.reduce(getSum).toFixed(2) + " KWH</div></h3>" + 
			"</div>"+
			"</div>"+
			"<div id='hvac'>"+
			"<div class='title'>"+
			"<h1>HVAC Consumption</h1>"+
			"</div>"+
			"<div class='max'>"+
			"<h2>Max <div class='num'>" + Math.max(...HVAC_data) + " KW</div></h2>"+
			"</div>"+
			"<div class='min'>"+
			"<h3>Total <div class='num'>" + HVAC_data.reduce(getSum).toFixed(2) + " KWH</div></h3>"+
			"</div>"+
			"</div>";



			RGraph.ObjectRegistry.clear(document.getElementById('cvs'));
			RGraph.reset(document.getElementById('cvs'));
			
			var line = new RGraph.Line({
				id: 'cvs',
				data: [Grid_data, HVAC_data,Solar_data],
				options: {
					filled: true,
					filledAccumulative: false,
					colors: ['rgba(67,67,67, 0.5)','rgba(218,100,58,0.5)','rgba(7, 181, 7,0.5)'],
					spline: false,
					shadow: false,
					textAccessible: true,
					key: ['Grid Consumption','HVAC Consumption','Solar Generation'],
					xmin: seven_day_start_date.getTime(),
					xmax: seven_day_end_date.getTime(),
					line: true,
					linewidth: 1,
					lineStepped: [false, true],
					
					backgroundGridAutofitNumhlines: 7,
					backgroundGridAutofitNumvlines: 7,
					unitsPost: 'KW',
					lineShadowColor: '#999',
					lineShadowBlur: 15,
					lineShadowOffsetx: 0,
					lineShadowOffsety: 0,
					noxaxis: true,
					tickmarks: null,
					gutterLeft: 50,
					gutterBottom: 50,
					textAccessible: true
				}
			}).draw();		
	
	
/*			var xaxis = new RGraph.Drawing.XAxis({
					id: 'cvs',
					y: line.canvas.height - line.gutterBottom,
					options: {
						labels: ['<?=date("D",strtotime('-6 days',time()))?>\n<?=date("d-m-Y",strtotime('-6 days',time()))?>','<?=date("D",strtotime('-5 days',time()))?>\n<?=date("d-m-Y",strtotime('-5 days',time()))?>','<?=date("D",strtotime('-4 days',time()))?>\n<?=date("d-m-Y",strtotime('-4 days',time()))?>','<?=date("D",strtotime('-3 days',time()))?>\n<?=date("d-m-Y",strtotime('-3 days',time()))?>','<?=date("D",strtotime('-2 days',time()))?>\n<?=date("d-m-Y",strtotime('-2 days',time()))?>','<?=date("D",strtotime('-1 days',time()))?>\n<?=date("d-m-Y",strtotime('-1 days',time()))?>','<?=date("D")?>\n<?=date("d-m-Y")?>'],
						labelsPosition: 'section',
						textAccessible: true,
						textSize: 14
					}
				}).draw();	
*/
			setTimeout(function(){update_screen()},15000);
			break;
		case 6:
			$('html').css('background-image', 'url(images/BG5.jpg)');
	
			document.body.innerHTML = "";
			current_screen++;
			setTimeout(function(){update_screen()},5000);
			break;
		case 7:
			$('html').css('background-image', 'url(images/BG4.jpg)');
			
			document.body.innerHTML = "";
			current_screen = 1;
			setTimeout(function(){update_screen()},5000);
			break;

		default:
		   setTimeout(function(){update_screen()},5000);
		}
	}

	</script>
	

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	
<!-- <script src="./RGraph/libraries/RGraph.common.core.js"></script>
	 <script src="./RGraph/libraries/RGraph.common.key.js"></script>
	 <script src="./RGraph/libraries/RGraph.bar.js"></script>
	 <script src="./RGraph/libraries/RGraph.line.js"></script>
	 <script src="./RGraph/libraries/RGraph.drawing.xaxis.js"></script>
-->


	
	<script>
		$(document).ready(function ()
		{
			get_Site_Data(inputs,'Today',300000);	
			update_screen();
		});	
	</script>

	
	<body style="padding:0; margin:0px;">
	</body>
</html>



