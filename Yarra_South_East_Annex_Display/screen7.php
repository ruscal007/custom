<?php
//  
	header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");
	
	?>

<?
	
	
	$outpost_id = $_REQUEST['outpost_id'];

	
	$dateFrom = date("d/M/Y+00:00:00",strtotime('-6 days',time()));
	
//	$dateFrom = date("d/M/Y+00:00:00");
	$dateTo = date("d/M/Y+00:00:00",strtotime('+1 days',time()));

	function roundToQuarterHour($timestring) {
		$minutes = date('i', strtotime($timestring));
		return $minutes - ($minutes % 15);
	}

// set feed URL

$url = "https://www.outpostcentral.com/api/1.0/dataservice/?";
$url .= "userName=BLPPA_Display&password=BLPPA_Display&dateFrom=". $dateFrom ."&dateTo=". $dateTo ."&outpostID=".$outpost_id."&format=xml";
//echo $url . "\n";
$xmlDoc=new DOMDocument(); 

if (@$xmlDoc->load($url) === false) {
?>
	<script src="./RGraph/libraries/RGraph.common.core.js"></script>
	<script src="./RGraph/libraries/RGraph.bar.js"></script>
	<script src="./RGraph/libraries/RGraph.common.key.js"></script>
	<!DOCTYPE html
		PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
		"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
	<title>BLPPA Display</title>

		<style type="text/css">
			body, p{
				font-family:Calibri, verdana;
				font-size:16px;
			}
			body {
			  background: url(images/BG4.jpg) no-repeat center center fixed; 
			  -webkit-background-size: cover;
			  -moz-background-size: cover;
			  -o-background-size: cover;
			  background-size: cover;				
			}		
			
			#main {
				position: absolute;
				left: 260px;
				top: 130px;
			}
			
			#solar {
				position: absolute;
				left: 825px;
				top: 640px;
			}
			#grid {
				position: absolute;
				left: 825px;
				top: 708px;
			}
			#hvac {
				position: absolute;
				left: 825px;
				top: 775px;
			}
			h1 {
				font-size: 19pt;
			}
			</style>
	</head>
<body style="padding:0; margin:0px;">

				<div id="main">
					<h1>Cannot Connect to Online Server</h1>
				</div>

<?
} else {
	$x = $xmlDoc->documentElement;
	
	$readings = Array();
	
	$KWH_Meter_Read['KWH_Total_HVAC'] = Array();
	$KWH_Meter_Read['KWH_Total_GRID'] = Array();
	$KWH_Meter_Read['KWH_Total_Export_Solar'] = Array();
	
	$Grid_data = "";
	$HVAC_data = "";
	$Solar_data = "";
	
	$Solar_min = null;
	$Solar_max = null;
	$Solar_min_string = null;
	$Solar_max_string = null;
	
	
	$Grid_min = null;
	$Grid_max = null;
	$Grid_max_string = null;
	$Grid_min_string = null;
	
	$HVAC_min = null;
	$HVAC_max = null;
	$HVAC_min_string = null;
	$HVAC_max_string = null;
	
	
	$dateFrom = str_replace("+", " ", $dateFrom);
	$dateFrom = str_replace("/", "-", $dateFrom);
	
	$dateTo = str_replace("+", " ", $dateTo);
	$dateTo = str_replace("/", "-", $dateTo);
	
	foreach ($x->getElementsByTagName('site') as $site) {
		$names = $site->getElementsByTagName("name");
		$site_name = $names->item(0)->nodeValue;
	}

	foreach ($x->getElementsByTagName('logger') as $logger) {
		$last_log = $logger->getElementsByTagName("last_telemetry");
		$last_log_dt = $last_log->item(0)->nodeValue;
	}
	
	echo $last_log_dt;
	
	foreach ($x->getElementsByTagName('input') as $input) {
	//  print $item->nodeName . " = " . $item->nodeValue . "<br>";
		$names = $input->getElementsByTagName("name");
		$name = $names->item(0)->nodeValue;
	
		if ($name == 'KW_Interval_Grid') {
			//print $name . "</BR>";
	
			foreach ($input->getElementsByTagName("record") as $record) {
	
				$date_times = $record->getElementsByTagName("date");
				$date_time = $date_times->item(0)->nodeValue;
				
				$values = $record->getElementsByTagName("value");
				$value = $values->item(0)->nodeValue;
				
				$date_time = str_replace("T", " ", $date_time);
				$readings[$name][$date_time] = $value;
				
				
				
			}
		}
	
		if ($name == 'KWH_Daily_Grid') {
			//print $name . "</BR>";
	
			foreach ($input->getElementsByTagName("record") as $record) {
	
				$date_times = $record->getElementsByTagName("date");
				$date_time = $date_times->item(0)->nodeValue;
				
				$values = $record->getElementsByTagName("value");
				$value = $values->item(0)->nodeValue;
				
				$date_time = str_replace("T", " ", $date_time);
				$readings[$name][date("Y-m-d",strtotime($date_time))] = $value;
				
				if ($value > $Grid_max) {
					$Grid_max = $value;
					$Grid_max_string = " " . $value . " KWH";
				}
				if ($value < $Grid_min || is_null($Grid_min)) {
					$Grid_min = $value;
					$Grid_min_string = " " . $value . " KWH";
				}
				
			}
		}
	
		if ($name == 'KWH_Daily_HVAC') {
			//print $name . "</BR>";
	
			foreach ($input->getElementsByTagName("record") as $record) {
	
				$date_times = $record->getElementsByTagName("date");
				$date_time = $date_times->item(0)->nodeValue;
				
				$values = $record->getElementsByTagName("value");
				$value = $values->item(0)->nodeValue;
				
				$date_time = str_replace("T", " ", $date_time);
				$readings[$name][date("Y-m-d",strtotime($date_time))] = $value;
				
				if ($value > $HVAC_max) {
					$HVAC_max = $value;
					$HVAC_max_string = " " . $value . " KWH";
				}
				if ($value < $HVAC_min || is_null($HVAC_min)) {
					$HVAC_min = $value;
					$HVAC_min_string = " " . $value . " KWH";
				}
	
				
			}
		}
	
		if ($name == 'KWH_Daily_Export_Solar') {
			//print $name . "</BR>";
	
			foreach ($input->getElementsByTagName("record") as $record) {
	
				$date_times = $record->getElementsByTagName("date");
				$date_time = $date_times->item(0)->nodeValue;
				
				$values = $record->getElementsByTagName("value");
				$value = $values->item(0)->nodeValue;
				
				$date_time = str_replace("T", " ", $date_time);
				$readings[$name][date("Y-m-d",strtotime($date_time))] = $value;
	
				if ($value > $Solar_max) {
					$Solar_max = $value;
					$Solar_max_string = " " . $value . " KWH";
				}
				if ($value < $Solar_min || is_null($Solar_min)) {
					$Solar_min = $value;
					$Solar_min_string = " " . $value . " KWH";
				}
				
			}
		}	
		
		
		if ($name == 'KWH_Interval_Grid') {
			//print $name . "</BR>";
	
			foreach ($input->getElementsByTagName("record") as $record) {
	
				$date_times = $record->getElementsByTagName("date");
				$date_time = $date_times->item(0)->nodeValue;
				
				$values = $record->getElementsByTagName("value");
				$value = $values->item(0)->nodeValue;
				
				$date_time = str_replace("T", " ", $date_time);
				$readings[$name][$date_time] = $value;
				
				
			}
		}
	
		if ($name == 'KWH_Interval_HVAC') {
			//print $name . "</BR>";
	
			foreach ($input->getElementsByTagName("record") as $record) {
	
				$date_times = $record->getElementsByTagName("date");
				$date_time = $date_times->item(0)->nodeValue;
				
				$values = $record->getElementsByTagName("value");
				$value = $values->item(0)->nodeValue;
				
				$date_time = str_replace("T", " ", $date_time);
				$readings[$name][$date_time] = $value;
			}
		}
	
		if ($name == 'KWH_Interval_Export_Solar') {
			//print $name . "</BR>";
	
			foreach ($input->getElementsByTagName("record") as $record) {
	
				$date_times = $record->getElementsByTagName("date");
				$date_time = $date_times->item(0)->nodeValue;
				
				$values = $record->getElementsByTagName("value");
				$value = $values->item(0)->nodeValue;
				
				$date_time = str_replace("T", " ", $date_time);
				$readings[$name][$date_time] = $value;
			}
		}
		
		if ($name == 'KW_Interval_HVAC') {
			//print $name . "</BR>";
			for ($i=strtotime($dateFrom);$i<=strtotime($dateTo);$i=$i+900 ) {
				$readings[$name][$date_time] = 'abc';
			}
			
			foreach ($input->getElementsByTagName("record") as $record) {
				$date_times = $record->getElementsByTagName("date");
				$date_time = $date_times->item(0)->nodeValue;
				
				$values = $record->getElementsByTagName("value");
				$value = $values->item(0)->nodeValue;
				
				$date_time = str_replace("T", " ", $date_time);
				$readings[$name][$date_time] = $value;
	
				
			}
			$HVAC_data = "[";
			foreach($readings[$name] as $date_time => $value){
				$HVAC_data .= $value . ",";
			}		
			$HVAC_data = rtrim($HVAC_data,",");
			$HVAC_data .= "]";	
			
		}
	
		if ($name == 'KW_Interval_Export_Solar') {
			//print $name . "</BR>";
	
			for ($i=strtotime($dateFrom);$i<=strtotime($dateTo);$i=$i+900 ) {
				$readings[$name][$date_time] = 'null';
			}
			
			foreach ($input->getElementsByTagName("record") as $record) {
				$date_times = $record->getElementsByTagName("date");
				$date_time = $date_times->item(0)->nodeValue;
				
				$values = $record->getElementsByTagName("value");
				$value = $values->item(0)->nodeValue;
				
				$date_time = str_replace("T", " ", $date_time);
				$readings[$name][$date_time] = $value;
	
	
				
				
			}
			$Solar_data = "[";
			foreach($readings[$name] as $date_time => $value){
				$Solar_data .= $value . ",";
			}		
			$Solar_data = rtrim($Solar_data, ",");	
			$Solar_data .= "]";	
			
		}
	}
	
		$KWH_Day_Data = Array();
		$Grid_data = "";
		$HVAC_data = "";
		$Solar_data = "";
		$previous_day = "";
	
		$bar_data = Array();
		
		
		
		for ($i=strtotime($dateFrom);$i<=strtotime($dateTo);$i=$i+86400 ) {
			$day_data = Array();	
			
			if (is_numeric($readings['KWH_Daily_Export_Solar'][date('Y-m-d',$i)])) {
				$day_data[] =  $readings['KWH_Daily_Export_Solar'][date('Y-m-d',$i)] . ",";	
			} else {
				$day_data[] = 0;	
			}
			
			if (is_numeric($readings['KWH_Daily_Grid'][date('Y-m-d',$i)])) {
				$day_data[] =  $readings['KWH_Daily_Grid'][date('Y-m-d',$i)] . ",";	
			} else {
				$day_data[] = 0;	
			}
			if (is_numeric($readings['KWH_Daily_HVAC'][date('Y-m-d',$i)])) {
				$day_data[] =  $readings['KWH_Daily_HVAC'][date('Y-m-d',$i)] . ",";	
			} else {
				$day_data[] = 0;	
			}
			$bar_data[] = $day_data;
		}
		array_pop($bar_data);
		
		$Solar_data = rtrim($Solar_data,",");	
		$HVAC_data = rtrim($HVAC_data,",");
		$Grid_data = rtrim($Grid_data,",");
		
	$smallest_date = false;
	$largest_date = false;

//print_r(json_encode($bar_data));
?>
	<script src="./RGraph/libraries/RGraph.common.core.js"></script>
	<script src="./RGraph/libraries/RGraph.bar.js"></script>
	<script src="./RGraph/libraries/RGraph.common.key.js"></script>
	<!DOCTYPE html
		PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
		"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
	<title>BLPPA Display</title>

		<style type="text/css">
			body, p{
				font-family:Calibri, verdana;
				font-size:16px;
			}
			body {
			  background: url(images/BG4.jpg) no-repeat center center fixed; 
			  -webkit-background-size: cover;
			  -moz-background-size: cover;
			  -o-background-size: cover;
			  background-size: cover;				
			}		
			
			#title {
				position: absolute;
				top: 50px;
				width: 100%;
			}

			#title h1{
				color: blue;
				text-align:center;
				
			}
			
			
			#main {
				position: absolute;
				left: 260px;
				top: 140px;
			}
			
			#solar {
				position: absolute;
				left: 825px;
				top: 640px;
				width : 870px;
			}
			#grid {
				position: absolute;
				left: 825px;
				top: 708px;
				width : 870px;
			}
			#hvac {
				position: absolute;
				left: 825px;
				top: 775px;
				width : 870px;
			}
			#solar h1, #grid h1, #hvac h1{
				font-size: 19pt;
				color: grey;
				display:inline;
			}
			#solar h2, #grid h2, #hvac h2{
				font-size: 17pt;
				color: grey;
				display:inline;
			}
			#solar h3, #grid h3, #hvac h3{
				font-size: 17pt;
				color: grey;
				display:inline;
			}

			.title {
				display:inline-block;
			}
			
			.max {
				position:absolute;
				display:inline-block;
				left: 450px;
			}
			
			.min {
				position:absolute;
				display:inline-block;
				left: 700px;
			}

			.max.num {
				position:absolute;
				display:inline-block;
				left: 500px;
			}
			
			.min.num {
				position:absolute;
				display:inline-block;
				left: 600px;
			}

			</style>
	</head>
<body style="padding:0; margin:0px;">
				<div id="title">
					<h1><?=$site_name?> kWH Profile Past 7 Days</h1>
				</div>
				<div id="main">
					<canvas  id="cvs" width='1450' height='430'>
						[No canvas support]
					</canvas>
				</div>
				<div id="solar">
					<div class="title">
						<h1>Solar Production</h1>
					</div>
					<div class="max">
						<h2>Max <div class="num"><?=$Solar_max_string?></div></h2>
					</div>
					<div class="min">
						<h3>Min <div class="num"><?=$Solar_min_string?></div></h3> 
					</div>
				</div>
				<div id="grid">
					<div class="title">
						<h1>Grid Consumption</h1>
					</div>
					<div class="max">
						<h2>Max <div class="num"><?=$Grid_max_string?></div></h2>
					</div>
					<div class="min">
						<h3>Min <div class="num"><?=$Grid_min_string?></div></h3> 
					</div>
				</div>
				<div id="hvac">
					<div class="title">
						<h1>HVAC Consumption</h1>
					</div>
					<div class="max">
						<h2>Max <div class="num"><?=$HVAC_max_string?></div></h2>
					</div>
					<div class="min">
						<h3>Min <div class="num"><?=$HVAC_min_string?></div></h3> 
					</div>
				</div>
<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.1.1.min.js"></script>
<script>
    $(document).ready(function ()
    {
        var bar = new RGraph.Bar({
            id: 'cvs',
            data: <?=json_encode($bar_data)?>,
            options: {
                labels: ['<?=date("D",strtotime('-6 days',time()))?>\n<?=date("d-m-Y",strtotime('-6 days',time()))?>','<?=date("D",strtotime('-5 days',time()))?>\n<?=date("d-m-Y",strtotime('-5 days',time()))?>','<?=date("D",strtotime('-4 days',time()))?>\n<?=date("d-m-Y",strtotime('-4 days',time()))?>','<?=date("D",strtotime('-3 days',time()))?>\n<?=date("d-m-Y",strtotime('-3 days',time()))?>','<?=date("D",strtotime('-2 days',time()))?>\n<?=date("d-m-Y",strtotime('-2 days',time()))?>','<?=date("D",strtotime('-1 days',time()))?>\n<?=date("d-m-Y",strtotime('-1 days',time()))?>','<?=date("D")?>\n<?=date("d-m-Y")?>'],
                colors: ['rgba(7, 181, 7,0.5)','rgba(67,67,67, 0.5)','rgba(218,100,58,0.5)'],
				key: ['Solar Generation','Grid Consumption','HVAC Consumption'],
				hmargin: 25,
                hmarginGrouped: 3,
                backgroundGridAutofitNumvlines: 5,
                shadowOffsetx: 2,
                shadowOffsety: 2,
                shadowBlur: 2,
				unitsPost: 'KWH',
                textAccessible: true
            }
        }).draw();
    });	
	
</script>
<?
};	
 ?>
<script>setTimeout(function(){window.location.href='screen7.php?outpost_id=<?=$_REQUEST['outpost_id']?>'},15000);</script>
</body>
</html>



