Outpost ID: OP30852
Start and end dates: 01-01-70 10:00:24 am - 01-01-70 10:00:31 am
URL: https://www.outpostcentral.com/api/1.0/dataservice/?userName=BLPPA_Display&password=BLPPA_Display&dateFrom=24/May/2017+00:00:00&dateTo=31/May/2017+00:00:00&outpostID=OP30852&format=xml
Signal
KWH_Total_Grid
KWH_Total_HVAC
KWH_Total_Export_Solar
KWH_Total_Import_Solar
KWH_Daily_Export_Solar
KWH_Daily_Grid
KWH_Daily_HVAC
KWH_Total_Export_Grid
KWH_Daily_Export_Grid
KWH_Interval_Solar_Consumed_Onsite
KWH_Daily_Solar_Consumed_Onsite
KWH_Interval_Export_Grid
KWH_Interval_Export_Solar
KWH_Interval_Grid
KWH_Interval_HVAC
KWH_Interval_Import_Solar
KWH_Interval_Export_Grid_RAW
PIC
Battery
Ontime
Pic Ontime
PIC Battery Bulk
Battery Voltage
External Voltage
Battery Temperature
Coulomb Count
Charging Status
mAh Used
Coulomb Status
CC Operating Mode
M2A512S64
M3A512S64
WHrs_Export_Solar
WHrs_Import_Solar
active power phase C
active power phase A
active power phase B
M3A524S32
KW_Interval_Export_Grid
KW_Interval_Export_Solar
KW_Interval_Grid
KW_Interval_HVAC
KW_Interval_Import_Solar
KW_Interval_Solar_Consumed_Onsite
