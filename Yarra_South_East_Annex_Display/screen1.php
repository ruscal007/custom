<?php
//  
	header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");
	
	?>

<?
	
	$dateFrom = date("d/M/Y+00:00:00",time());
	
//	$dateFrom = date("d/M/Y+00:00:00");
	$dateTo = date("d/M/Y+23:59:59",time());

	function roundToQuarterHour($timestring) {
		$minutes = date('i', strtotime($timestring));
		return $minutes - ($minutes % 15);
	}

// set feed URL

$url = "https://www.outpostcentral.com/api/1.0/dataservice/?";
$url .= "userName=BLPPA_Display&password=BLPPA_Display&dateFrom=". $dateFrom ."&dateTo=". $dateTo ."&outpostID=op34987&format=xml";
//echo $url . "\n";
$xmlDoc=new DOMDocument(); 

if (@$xmlDoc->load($url) === false) {
?>
	<script src="./RGraph/libraries/RGraph.common.core.js"></script>
	<script src="./RGraph/libraries/RGraph.bar.js"></script>
	<script src="./RGraph/libraries/RGraph.common.key.js"></script>
	<!DOCTYPE html
		PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
		"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
	<title>BLPPA Display</title>

		<style type="text/css">
			body, p{
				font-family:Calibri, verdana;
				font-size:16px;
			}
			body {
			  background: url(images/BG1.jpg) no-repeat center center fixed; 
			  -webkit-background-size: cover;
			  -moz-background-size: cover;
			  -o-background-size: cover;
			  background-size: cover;				
			}		
			
			#main {
				position: absolute;
				left: 260px;
				top: 250px;
			}
			
			#solar {
				position: absolute;
				left: 825px;
				top: 640px;
			}
			#grid {
				position: absolute;
				left: 825px;
				top: 708px;
			}
			#hvac {
				position: absolute;
				left: 825px;
				top: 775px;
			}
			h1 {
				font-size: 19pt;
			}
			</style>
	</head>
<body style="padding:0; margin:0px;">

				<div id="main">
					<h1>Cannot Connect to Online Server</h1>
				</div>

<?
} else {
	$x = $xmlDoc->documentElement;
	
	$readings = Array();
	
	$Grid_data = "";
	$HVAC_data = "";
	$Solar_data = "";
	
	
	$dateFrom = str_replace("+", " ", $dateFrom);
	$dateFrom = str_replace("/", "-", $dateFrom);
	
	$dateTo = str_replace("+", " ", $dateTo);
	$dateTo = str_replace("/", "-", $dateTo);
	
	foreach ($x->getElementsByTagName('site') as $site) {
		$names = $site->getElementsByTagName("name");
		$site_name = $names->item(0)->nodeValue;
	}
		
	foreach ($x->getElementsByTagName('input') as $input) {
	//  print $item->nodeName . " = " . $item->nodeValue . "<br>";
		$names = $input->getElementsByTagName("name");
		$name = $names->item(0)->nodeValue;
	
		if ($name == 'KWH_Daily_Battery') {
			//print $name . "</BR>";
	
			foreach ($input->getElementsByTagName("record") as $record) {
	
				$date_times = $record->getElementsByTagName("date");
				$date_time = $date_times->item(0)->nodeValue;
				
				$values = $record->getElementsByTagName("value");
				$value = $values->item(0)->nodeValue;
				
				$date_time = str_replace("T", " ", $date_time);
				$readings[$name] = $value;
			}
		}
	
		if ($name == 'KWH_Daily_Battery_Export') {
			//print $name . "</BR>";
	
			foreach ($input->getElementsByTagName("record") as $record) {
	
				$date_times = $record->getElementsByTagName("date");
				$date_time = $date_times->item(0)->nodeValue;
				
				$values = $record->getElementsByTagName("value");
				$value = $values->item(0)->nodeValue;
				
				$date_time = str_replace("T", " ", $date_time);
				$readings[$name] = $value;
			}
		}
	
		if ($name == 'KWH_Daily_Grid') {
			//print $name . "</BR>";
	
			foreach ($input->getElementsByTagName("record") as $record) {
	
				$date_times = $record->getElementsByTagName("date");
				$date_time = $date_times->item(0)->nodeValue;
				
				$values = $record->getElementsByTagName("value");
				$value = $values->item(0)->nodeValue;
				
				$date_time = str_replace("T", " ", $date_time);
				$readings[$name] = $value;
			}
		}
	
		if ($name == 'KWH_Daily_Grid_Export') {
			//print $name . "</BR>";
	
			foreach ($input->getElementsByTagName("record") as $record) {
	
				$date_times = $record->getElementsByTagName("date");
				$date_time = $date_times->item(0)->nodeValue;
				
				$values = $record->getElementsByTagName("value");
				$value = $values->item(0)->nodeValue;
				
				$date_time = str_replace("T", " ", $date_time);
				$readings[$name] = $value;
			}
		}	
		
		
		if ($name == 'KWH_Daily_HVAC') {
			//print $name . "</BR>";
	
			foreach ($input->getElementsByTagName("record") as $record) {
	
				$date_times = $record->getElementsByTagName("date");
				$date_time = $date_times->item(0)->nodeValue;
				
				$values = $record->getElementsByTagName("value");
				$value = $values->item(0)->nodeValue;
				
				$date_time = str_replace("T", " ", $date_time);
				$readings[$name] = $value;
				$readings['Total_Use'] += $value;
			}
		}
	
		if ($name == 'KWH_Daily_Lighting') {
			//print $name . "</BR>";
	
			foreach ($input->getElementsByTagName("record") as $record) {
	
				$date_times = $record->getElementsByTagName("date");
				$date_time = $date_times->item(0)->nodeValue;
				
				$values = $record->getElementsByTagName("value");
				$value = $values->item(0)->nodeValue;
				
				$date_time = str_replace("T", " ", $date_time);
				$readings[$name] = $value;
				$readings['Total_Use'] += $value;
			}
		}
	
		if ($name == 'KWH_Daily_Power') {
			//print $name . "</BR>";
	
			foreach ($input->getElementsByTagName("record") as $record) {
	
				$date_times = $record->getElementsByTagName("date");
				$date_time = $date_times->item(0)->nodeValue;
				
				$values = $record->getElementsByTagName("value");
				$value = $values->item(0)->nodeValue;
				
				$date_time = str_replace("T", " ", $date_time);
				$readings[$name] = $value;
				$readings['Total_Use'] += $value;
			}

		}
		
		if ($name == 'KWH_Daily_Solar') {
			//print $name . "</BR>";
			
			foreach ($input->getElementsByTagName("record") as $record) {
				$date_times = $record->getElementsByTagName("date");
				$date_time = $date_times->item(0)->nodeValue;
				
				$values = $record->getElementsByTagName("value");
				$value = $values->item(0)->nodeValue;
				
				$date_time = str_replace("T", " ", $date_time);
				$readings[$name] = $value;
			}
		}
	}
	
		$KWH_Day_Data = Array();
		$Grid_data = "";
		$HVAC_data = "";
		$Solar_data = "";
		$previous_day = "";
	
		$bar_data = Array();
		$day_data = Array();
		
		$graph_key = "[";
		$graph_colours = "[";
		
		if (is_numeric($readings['KWH_Daily_Grid'])) {
			$day_data[] =  $readings['KWH_Daily_Grid'];	
		} else {
			$day_data[] = 0;
		}
		$graph_key .= "'Total Use',";
		$graph_colours .= "'rgba(0, 0, 0,1)',";


		if (is_numeric($readings['KWH_Daily_HVAC'])) {
			$day_data[] =  $readings['KWH_Daily_HVAC'];	
		} else {
			$day_data[] = 0;
		}
		$graph_key .= "'HVAC',";
		$graph_colours .= "'rgba(255, 208, 52,1)',";
		
		if (is_numeric($readings['KWH_Daily_Lighting'])) {
			$day_data[] =  $readings['KWH_Daily_Lighting'];	
		} else {
			$day_data[] = 0;
		}
		$graph_key .= "'Lighting',";
		$graph_colours .= "'rgba(255, 76, 59,1)',";
		
		if (is_numeric($readings['KWH_Daily_Power'])) {
			$day_data[] =  $readings['KWH_Daily_Power'];	
		} else {
			$day_data[] = 0;
		}
		$graph_key .= "'Power',";		
		$graph_colours .= "'rgba(198, 200, 202,1)',";
		
//		if (is_numeric($readings['KWH_Daily_Battery'])) {
//			$day_data[] =  $readings['KWH_Daily_Battery'];	
//		} else {
//			$day_data[] = 0;
//		}
//		$graph_key += "'Battery Charge',";
// 		$graph_colours += "'rgba(7, 181, 7,0.5)',";

		if (is_numeric($readings['KWH_Daily_Battery_Export'])) {
			$day_data[] =  $readings['KWH_Daily_Battery_Export'];	
		} else {
			$day_data[] = 0;
		}
		$graph_key .= "'Battery Discharge',"		;
		$graph_colours .= "'rgba(0, 114, 187,1)',";		
		
		if (is_numeric($readings['KWH_Daily_Solar'])) {
			$day_data[] =  $readings['KWH_Daily_Solar'];	
		} else {
			$day_data[] = 0;
		}
		$graph_key .= "'Solar Generation'";
		$graph_colours .= "'rgba(50, 185, 45,1)'";
		
		$graph_key .= "]";
		$graph_colours .= "]";
		
		$bar_data[] = $day_data;

	$smallest_date = false;
	$largest_date = false;

//print_r(json_encode($bar_data));
?>
	<script src="./RGraph/libraries/RGraph.common.core.js"></script>
	<script src="./RGraph/libraries/RGraph.bar.js"></script>
	<script src="./RGraph/libraries/RGraph.common.key.js"></script>
	<!DOCTYPE html
		PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
		"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
	

		<style type="text/css">
			body, p{
				font-family:Calibri, verdana;
				font-size:16px;
			}
			body {
			  background: url(images/BG1.jpg) no-repeat center center fixed; 
			  -webkit-background-size: cover;
			  -moz-background-size: cover;
			  -o-background-size: cover;
			  background-size: cover;				
			}		
			
			#title {
				position: absolute;
				top: 100px;
				width: 100%;
			}

			#title h1{
				color: blue;
				text-align:center;
				
			}
			
			
			#main {
				position: absolute;
				left: 200px;
				top: 150px;
			}
			
			#solar {
				position: absolute;
				left: 825px;
				top: 640px;
				width : 870px;
			}
			#grid {
				position: absolute;
				left: 825px;
				top: 708px;
				width : 870px;
			}
			#hvac {
				position: absolute;
				left: 825px;
				top: 775px;
				width : 870px;
			}
			#solar h1, #grid h1, #hvac h1{
				font-size: 19pt;
				color: grey;
				display:inline;
			}
			#solar h2, #grid h2, #hvac h2{
				font-size: 17pt;
				color: grey;
				display:inline;
			}
			#solar h3, #grid h3, #hvac h3{
				font-size: 17pt;
				color: grey;
				display:inline;
			}

			.title {
				display:inline-block;
			}
			
			.max {
				position:absolute;
				display:inline-block;
				left: 450px;
			}
			
			.min {
				position:absolute;
				display:inline-block;
				left: 700px;
			}

			.max.num {
				position:absolute;
				display:inline-block;
				left: 500px;
			}
			
			.min.num {
				position:absolute;
				display:inline-block;
				left: 600px;
			}

			</style>
	</head>
<body style="padding:0; margin:0px;">
				<div id="title">
					<h1><?=$site_name?> Todays Energy</h1>
				</div>
				<div id="main">
					<canvas  id="cvs" width='1550' height='720'>
						[No canvas support]
					</canvas>
				</div>
<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.1.1.min.js"></script>
<script>
    $(document).ready(function ()
    {
        var bar = new RGraph.Bar({
            id: 'cvs',
            data: <?=json_encode($bar_data)?>,
            options: {
                labels: ['<?=date("D")?>\n<?=date("d-m-Y")?>'],
                colors: <?=$graph_colours?>,
				key: <?=$graph_key?>,
				keyTextSize : 15,
				hmargin: 25,
                hmarginGrouped: 3,
                backgroundGridAutofitNumvlines: 5,
                shadowOffsetx: 2,
                shadowOffsety: 2,
                shadowBlur: 2,
				unitsPost: 'KWH',
                textAccessible: true
            }
        }).draw();
		
    });	
	
</script>
<?
};	
 ?>
<script>setTimeout(function(){window.location.href='screen1.php'},15000);</script>
</body>
</html>



